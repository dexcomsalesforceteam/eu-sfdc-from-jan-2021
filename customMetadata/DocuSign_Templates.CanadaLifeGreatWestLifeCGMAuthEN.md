<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Canada Life (Great West Life) CGM AuthEN</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Email_Message__c</field>
        <value xsi:type="xsd:string">To assist Dexcom Canada with processing your insurance request, please complete the claim form using the link provided.</value>
    </values>
    <values>
        <field>Email_Subject__c</field>
        <value xsi:type="xsd:string">Please complete - Canada Life claim form</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Opportunity</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">CA_Opportunity</value>
    </values>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">40390cdd-0529-499e-9457-9130f11e53cc</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Canada Life (Great West Life) CGM Authorization Form EN</value>
    </values>
</CustomMetadata>
