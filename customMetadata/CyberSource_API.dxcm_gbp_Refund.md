<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Refund</label>
    <protected>false</protected>
    <values>
        <field>CurrencyIsoCode__c</field>
        <value xsi:type="xsd:string">GBP</value>
    </values>
    <values>
        <field>Enviornment__c</field>
        <value xsi:type="xsd:string">Prod</value>
    </values>
    <values>
        <field>endPoint__c</field>
        <value xsi:type="xsd:string">https://api.cybersource.com/pts/v2/payments/{PaymentId}/refunds</value>
    </values>
    <values>
        <field>host__c</field>
        <value xsi:type="xsd:string">apitest.cybersource.com</value>
    </values>
    <values>
        <field>keyid__c</field>
        <value xsi:type="xsd:string">a42c5bcb-b351-438c-8cd3-7beaddf7cff4</value>
    </values>
    <values>
        <field>merchant_id__c</field>
        <value xsi:type="xsd:string">dxcm_gbp</value>
    </values>
    <values>
        <field>profile_id__c</field>
        <value xsi:type="xsd:string">D4249A00-0CF8-4E57-8C31-2ACF9FF21D29</value>
    </values>
    <values>
        <field>reqBody__c</field>
        <value xsi:type="xsd:string">{&quot;clientReferenceInformation&quot;: {&quot;code&quot;: &quot;[OrderId]&quot;}, &quot;orderInformation&quot;: {&quot;amountDetails&quot;: {&quot;totalAmount&quot;: &quot;[RefundAmount]&quot;,&quot;currency&quot;: &quot;USD&quot;}}}</value>
    </values>
    <values>
        <field>secretKey__c</field>
        <value xsi:type="xsd:string">wAeB7+aEtz45LSg/1ixHTqtKoHWDWrmqPd5epUiVkC8=</value>
    </values>
</CustomMetadata>
