<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AT-1</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">AT</value>
    </values>
    <values>
        <field>HTML_Template_Name__c</field>
        <value xsi:type="xsd:string">AT Consumer Email</value>
    </values>
    <values>
        <field>Invoice_Name__c</field>
        <value xsi:type="xsd:string">AT_Invoice-PP_Fusion</value>
    </values>
    <values>
        <field>Key_Field_Value__c</field>
        <value xsi:type="xsd:string">AT Consumer</value>
    </values>
    <values>
        <field>Key_Field__c</field>
        <value xsi:type="xsd:string">Payor__r.RecordType.name, Account.RecordType.Name</value>
    </values>
</CustomMetadata>
