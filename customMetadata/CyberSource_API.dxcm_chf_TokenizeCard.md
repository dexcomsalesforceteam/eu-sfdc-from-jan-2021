<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TokenizeCard</label>
    <protected>false</protected>
    <values>
        <field>CurrencyIsoCode__c</field>
        <value xsi:type="xsd:string">CHF</value>
    </values>
    <values>
        <field>Enviornment__c</field>
        <value xsi:type="xsd:string">Prod</value>
    </values>
    <values>
        <field>endPoint__c</field>
        <value xsi:type="xsd:string">https://api.cybersource.com/tms/v1/instrumentidentifiers</value>
    </values>
    <values>
        <field>host__c</field>
        <value xsi:type="xsd:string">apitest.cybersource.com</value>
    </values>
    <values>
        <field>keyid__c</field>
        <value xsi:type="xsd:string">015b9de5-90f3-4b03-9a1c-572ec11d76b8</value>
    </values>
    <values>
        <field>merchant_id__c</field>
        <value xsi:type="xsd:string">dxcm_chf</value>
    </values>
    <values>
        <field>profile_id__c</field>
        <value xsi:type="xsd:string">F8835904-C21B-494C-82FC-2095E002345F</value>
    </values>
    <values>
        <field>reqBody__c</field>
        <value xsi:type="xsd:string">{&quot;card&quot;:{&quot;number&quot;:&quot;{0}&quot;}}</value>
    </values>
    <values>
        <field>secretKey__c</field>
        <value xsi:type="xsd:string">dEhwRj0zjK1F5S15Dkpz4b+DDXN+UTpLtxlSq3gXr0I=</value>
    </values>
</CustomMetadata>
