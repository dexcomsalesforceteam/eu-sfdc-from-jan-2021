<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NORDICINFU CARE AB (Denmark) - 1</label>
    <protected>false</protected>
    <values>
        <field>Account_Name__c</field>
        <value xsi:type="xsd:string">NORDICINFU CARE AB (Denmark)</value>
    </values>
    <values>
        <field>IFU__c</field>
        <value xsi:type="xsd:string">MT23414</value>
    </values>
    <values>
        <field>Pricebook__c</field>
        <value xsi:type="xsd:string">NORDICINFU CARE AB (Denmark) - Standard</value>
    </values>
    <values>
        <field>SKU__c</field>
        <value xsi:type="xsd:string">STK-GF-109</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">DIST STANDARD</value>
    </values>
</CustomMetadata>
