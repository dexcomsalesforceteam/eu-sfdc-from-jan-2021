<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sun Life 55555 Claim Form EN</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Email_Message__c</field>
        <value xsi:type="xsd:string">To assist Dexcom Canada with processing your insurance request, please complete the claim form using the link provided.</value>
    </values>
    <values>
        <field>Email_Subject__c</field>
        <value xsi:type="xsd:string">Please Complete - Sun Life Claim Form</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Opportunity</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">CA_Opportunity</value>
    </values>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">83e196e5-9335-4712-a349-cdfe0992e56b</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Sun Life 55555 Claim Form (EN)</value>
    </values>
</CustomMetadata>
