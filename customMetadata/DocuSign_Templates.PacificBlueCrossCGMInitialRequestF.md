<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pacific Blue Cross CGM Initial Request F</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Email_Message__c</field>
        <value xsi:type="xsd:string">To assist Dexcom Canada with processing your insurance request, please complete the claim form using the link provided.</value>
    </values>
    <values>
        <field>Email_Subject__c</field>
        <value xsi:type="xsd:string">Please complete - Pacific Blue Cross claim form</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Opportunity</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">CA_Opportunity</value>
    </values>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">e52f21cc-c34f-40a3-80c3-fe31d6e851dd</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Pacific Blue Cross CGM Initial Request Form</value>
    </values>
</CustomMetadata>
