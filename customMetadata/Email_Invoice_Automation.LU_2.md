<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LU-2</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">LU</value>
    </values>
    <values>
        <field>HTML_Template_Name__c</field>
        <value xsi:type="xsd:string">LU HTML Email</value>
    </values>
    <values>
        <field>Invoice_Name__c</field>
        <value xsi:type="xsd:string">LU German Invoice</value>
    </values>
    <values>
        <field>Key_Field_Value__c</field>
        <value xsi:type="xsd:string">de_DE</value>
    </values>
    <values>
        <field>Key_Field__c</field>
        <value xsi:type="xsd:string">Account.Preferred_Language__c</value>
    </values>
</CustomMetadata>
