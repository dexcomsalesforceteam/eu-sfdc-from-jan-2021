<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CA Certificate of Medical Necessity (EN)</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Email_Message__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Email_Subject__c</field>
        <value xsi:type="xsd:string">Please Docusign: Dexcom CMN 2021 (EN).pdf</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Opportunity</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">CA_Opportunity</value>
    </values>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">0dd99087-1ba7-415b-a02b-3273a46b2f37</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">CA Certificate of Medical Necessity (EN)</value>
    </values>
</CustomMetadata>
