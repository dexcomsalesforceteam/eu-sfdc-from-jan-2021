<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Refund</label>
    <protected>false</protected>
    <values>
        <field>CurrencyIsoCode__c</field>
        <value xsi:type="xsd:string">CAD</value>
    </values>
    <values>
        <field>Enviornment__c</field>
        <value xsi:type="xsd:string">Prod</value>
    </values>
    <values>
        <field>endPoint__c</field>
        <value xsi:type="xsd:string">https://api.cybersource.com/pts/v2/payments/{PaymentId}/refunds</value>
    </values>
    <values>
        <field>host__c</field>
        <value xsi:type="xsd:string">api.cybersource.com</value>
    </values>
    <values>
        <field>keyid__c</field>
        <value xsi:type="xsd:string">7c044c2e-583e-45c0-88f4-ba3b2f9c3e5e</value>
    </values>
    <values>
        <field>merchant_id__c</field>
        <value xsi:type="xsd:string">dxcm_cad</value>
    </values>
    <values>
        <field>profile_id__c</field>
        <value xsi:type="xsd:string">4724D46D-C996-41B3-9DA0-5F1BAC102435</value>
    </values>
    <values>
        <field>reqBody__c</field>
        <value xsi:type="xsd:string">{&quot;clientReferenceInformation&quot;: {&quot;code&quot;: &quot;[OrderId]&quot;}, &quot;orderInformation&quot;: {&quot;amountDetails&quot;: {&quot;totalAmount&quot;: &quot;[RefundAmount]&quot;,&quot;currency&quot;: &quot;CAD&quot;}}}</value>
    </values>
    <values>
        <field>secretKey__c</field>
        <value xsi:type="xsd:string">XLiqgarS3Jeons28va3gO95v6DaVjKCZE27yrN//MPY=</value>
    </values>
</CustomMetadata>
