public class ClsTaskTriggerHandler{

 public static void afterinsert(List<Task> triggerNew){
  //NS-1741 create  sample orders based on task 
  /***START*****/
   Set<Id> accountIds=new Set<Id>();
   for(Task task: triggerNew){
     if(task.Call_Reason__c=='Second Sensor Requested' && !String.IsBlank(task.WhatId) && (String.valueOf(task.WhatId).startsWithIgnoreCase('001'))){
	    accountIds.add(task.whatId);
     }
   }
   if(accountIds.size()>0){
       Map<id,account> accountMap=new Map<id,account>();
       List<Task> insertTaskList=new List<Task>();
       List<Account> updateAccountList=new List<Account>();
       for(Account act: [select id,name,Mktg_Journey_Touch_Points__c,ShippingCountryCode from account where id in :accountIds]){
         accountMap.put(act.id,act);
       }
       try{
       String secondSensorReq='Second Sensor Requested';
       for(Task task: triggerNew){
           Account updateAccount;
           Account act=accountMap.get(task.whatId);
 
          if(act.Mktg_Journey_Touch_Points__c==null || (act.Mktg_Journey_Touch_Points__c!=null && !act.Mktg_Journey_Touch_Points__c.contains(secondSensorReq))){
               system.debug('>>>>>>>>>enter to stage2');
              Map<string,string> result=ClsSampleProgramHandler.createOrder(task.whatId, act.ShippingCountryCode+' '+secondSensorReq,null,0,null);
              if(result.containsKey('Success')){
                  if(act.Mktg_Journey_Touch_Points__c !=null){
                      act.Mktg_Journey_Touch_Points__c=act.Mktg_Journey_Touch_Points__c+';'+secondSensorReq;
                      
                  }else act.Mktg_Journey_Touch_Points__c=secondSensorReq;
                  updateAccount=new account(id=task.whatId,Mktg_Journey_Touch_Points__c=act.Mktg_Journey_Touch_Points__c);
                  updateAccountList.add(updateAccount);
              }else{                  
                 ErrorUtility.LogError('Consume', 'ClsTaskTriggerHandler.afterinsert', result.get('Failed'), 'ClsTaskTriggerHandler', 'crm', '', 'Account ID: ' + act.id,'Fatal Error - Bailing out');
                 //create task when order already created 
                 insertTaskList.add(createTask(act.id,'Second Sensor Order Failed To Create',act.ShippingCountryCode));                
              
              }
           }else{
                 system.debug('>>>>>>>>>not eligible to create order');              
                 String error='Order creation failed because of Second sensor requested order already present for this account  ';
                 ErrorUtility.LogError('Consume', 'ClsTaskTriggerHandler.afterinsert', error, 'ClsTaskTriggerHandler', 'crm', '', 'Account ID: ' + act.id,'Fatal Error - Bailing out');
                 //create task when order already created 
                 insertTaskList.add(createTask(act.id,'Second requested Order is already present',act.ShippingCountryCode));
           }
           
       }
       if(updateAccountList.size()>0) update updateAccountList;             
       if(insertTaskList.size()>0) insert insertTaskList;
       
     }catch(Exception e){
         system.debug('ClsTaskTriggerHandler Error>'+'Line Number'+ e.getLineNumber()+'  Message'+e.getMessage());        
         ErrorUtility.LogError('Consume', 'ClsTaskTriggerHandler.afterinsert',e.getMessage()+'Line Number'+e.getLineNumber() , 'ClsTaskTriggerHandler', 'crm', '', '','Fatal Error - Bailing out');
     }
   }
 }
    
    public static Task createTask(Id whatId,string subject,string countrycode){
        Id devRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(countrycode+' Task').getRecordTypeId();
        Task tsk = new Task();
        tsk.Subject = subject;
        tsk.WhatId = whatid;
        tsk.Type = 'Other';
        tsk.recordtypeId=devRecordTypeId;
        return tsk;
       
    }
    //****END NS-1741****//

}