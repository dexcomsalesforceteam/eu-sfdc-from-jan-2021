/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created    : 18/12/2020
@Description    : Test class for OrderChangeEvent                 - 
****************************************************************************************************************/
@isTest
public class ClsOrderChangeEventTriggerHandlerTest {
	//create Test data
    @testSetup
    private static void setup(){
    //create Precriber account
    Account prescriberAccount = new Account();
    prescriberAccount.FirstName = 'Joe';
    prescriberAccount.LastName  = 'Prescriber';                                    		
	prescriberAccount.Gender__c =  'Male';
    prescriberAccount.ShippingStreet = 'Street1';
	prescriberAccount.ShippingCity ='Petersburger';
	prescriberAccount.ShippingPostalCode = '10247';
	prescriberAccount.ShippingState = 'Berlin';
	prescriberAccount.ShippingCountry = 'Germany';
	prescriberAccount.Prescriber_Id__c = '23333333';
	prescriberAccount.BillingStreet = 'Street1';
	prescriberAccount.BillingCity ='Petersburger';
	prescriberAccount.BillingPostalCode = '10247';
	prescriberAccount.BillingState = 'Berlin';
	prescriberAccount.BillingCountry = 'Germany';
	prescriberAccount.OK_Status__c = 'Valid';  
	prescriberAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('DE Prescriber').getRecordTypeId();
    insert prescriberAccount;
    //create Medical Facility
    Account medicalFacilityAccount = new Account();
    medicalFacilityAccount.Name  = 'Medical facility';
    medicalFacilityAccount.ShippingStreet = 'Street1';
    medicalFacilityAccount.ShippingCity ='Petersburger';
    medicalFacilityAccount.ShippingPostalCode = '10247';
	medicalFacilityAccount.ShippingState = 'Berlin';
	medicalFacilityAccount.ShippingCountry = 'Germany';
	medicalFacilityAccount.BillingStreet = 'Street1';
	medicalFacilityAccount.BillingCity ='Petersburger';
	medicalFacilityAccount.BillingPostalCode = '10247';
	medicalFacilityAccount.BillingState = 'Berlin';
	medicalFacilityAccount.BillingCountry = 'Germany';
	medicalFacilityAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('DE Medical Facility').getRecordTypeId();
    insert medicalFacilityAccount;

    //create a payor account    
    account accPayor = new Account();
    accPayor.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('DE Payor').getRecordTypeId();
    accPayor.CurrencyIsoCode = 'EUR';
    accPayor.Name = 'Test Payor 2';
    accPayor.Insurance_Company_Identification_Number__c = '1223444';
	accPayor.BillingStreet = 'Street1';
    accPayor.BillingCity ='Petersburger';
	accPayor.BillingPostalCode = '10247';
	accPayor.BillingState = 'Berlin';
	accPayor.BillingCountry = 'Germany';
    accPayor.ShippingStreet = 'Street1';
    accPayor.ShippingCity ='Petersburger';
    accPayor.ShippingPostalCode = '10247';
    accPayor.ShippingState = 'Berlin';
    accPayor.ShippingCountry = 'Germany';
    insert accPayor;
    //Create Pricebook
    Map<String, Id> customPricebookMap = ClsTestDataFactory.createCPB(new List <String>{'DE_Pricebook'}, 'EUR');
    String customPricebookId = customPricebookMap.get('DE_Pricebook');
    //Create Products
    Map<String, Id> products = ClsTestDataFactory.createProducts(new List<String> {'STK-GF-013', 'STT-GF-004'/*, 'DEX-SHIP-01'*/});       
    //Create Pricebook EntryPair
	Map<Id, Decimal> productIdToPriceMap = new Map<Id, Decimal>();
    for(String productName : products.keySet())
    	productIdToPriceMap.put(products.get(productName), 125.00);
		Map<Id, Id> productIdToPbeId = ClsTestDataFactory.createCustomPricebookEntries(productIdToPriceMap, customPricebookId);
		Id oppRecType = [Select Id from RecordType Where isActive = true AND DeveloperName = 'DE_Opportunity'].id;
        
	Opportunity newOpp = new Opportunity();
    newOpp.RecordTypeId = oppRecType;
    newOpp.Name = 'New Opportunity For Testing';
    newOpp.CurrencyIsoCode = 'EUR';
    newOpp.AccountId = prescriberAccount.Id;
    newOpp.Payor__c = accPayor.Id;
    newOpp.Type = 'New';
    newOpp.Contact_Letter_Sent__c = true;
    newOpp.Contact_Doc_Received__c = true;
	newOpp.Insurance_Doc_Received__c = true;
	newOpp.Hard_Copy_Out__c = true;
    newOpp.Hard_Copy_In__c = true;
    newOpp.Training_Letter_Sent__c = true;
    newOpp.Training_Letter_Sent_Date__c = Date.Today();
    newOpp.StageName = 'New Opportunity';
    newOpp.Training_Date__c = Date.today()+3;
    newOpp.Doc_Collection_Status__c = 'Docs On File - Pending' ;        
    newOpp.Email_Stage__c = null ; 
    newOpp.DexTracker_Email__c = 'Yes';
    newOpp.Consumer_Email__c = 'Tesdt@gmail.com';     
    newOpp.CloseDate = Date.Today().addDays(30);
    newOpp.Pricebook2Id = customPricebookId;
    insert newOpp;     
    //Insert Opportunity Products
    List<OpportunityLineItem> lineItemsToBeInserted = new List<OpportunityLineItem>();
    	for(String productName : products.keySet()) {
    		OpportunityLineItem newItem = new OpportunityLineItem();
        	newItem.OpportunityId = newOpp.Id;
      		newItem.Quantity  = 2;
      		newItem.UnitPrice = 125;
        	newItem.Repeat_Quantity__c = 12;
        	newItem.Repeat_after_in_Days__c = 90;
        	newItem.PricebookEntryId = productIdToPbeId.get(products.get(productName));
        	lineItemsToBeInserted.add(newItem);
		}
	insert lineItemsToBeInserted;
	//Create Orders
	Order newOrder = new Order();
	newOrder.AccountId = prescriberAccount.Id;
	newOrder.OpportunityId = newOpp.Id;
	newOrder.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('DE Sales Order').getRecordTypeId();
	newOrder.Type = ClsConstants.ORDER_TYPE_DE_STANDARD;	
    newOrder.EffectiveDate = System.today();
	newOrder.Price_Book__c = customPricebookId;
	newOrder.Pricebook2Id = customPricebookId;
	newOrder.Status = ClsConstants.ORDER_STATUS_DRAFT;
	insert newOrder;  
                Test.startTest();
	//List order line items
    List<OrderItem> orderItemList = new List<OrderItem>();
		for(Id pbeId : productIdToPbeId.values()){
            OrderItem oi = new OrderItem();
            oi.OrderId = newOrder.Id;
            oi.PricebookEntryId = pbeId;
            oi.Quantity = 1;
            oi.UnitPrice = 125.00;
            oi.Tracking_Number__c = '12345';
            oi.Product_Name__c = 'STK-GF-109' ;
            orderItemList.add(oi);
        }
		insert orderItemList;
		newOrder.shipping_hold_status__c = ClsConstants.ORDER_STATUS_COMPLETED;
        update newOrder;
        System.debug('&&&&&&&&&&&Order Status- Shipping Hold&&&&&&&&&&&&&&' + newOrder.Status);
        newOrder.Status =ClsConstants.ORDER_STATUS_ACTIVATED;
        update newOrder;
        System.debug('&&&&&&&&&&&Order Status- ACTIVATED&&&&&&&&&&&&&&' + newOrder.Status);
        Order newOrder1 = new Order();
        newOrder1.AccountId = prescriberAccount.Id;
        newOrder1.RecordTypeId = ClsConstants.ORDER_DE_SALES_ORDER_RECORDTYPE_ID;
        newOrder1.Type = ClsConstants.ORDER_TYPE_DE_STANDARD;
        newOrder1.OpportunityId = newOpp.Id;
        newOrder1.EffectiveDate = System.today();
        newOrder1.Price_Book__c = customPricebookId;
        newOrder1.Pricebook2Id = customPricebookId;
        newOrder1.Status = ClsConstants.ORDER_STATUS_DRAFT;
        insert newOrder1;
        List<OrderItem> orderItemList1 = new List<OrderItem>();
        for(Id pbeId : productIdToPbeId.values()){
            OrderItem oi = new OrderItem();
            oi.OrderId = newOrder1.Id;
            oi.PricebookEntryId = pbeId;
            oi.Quantity = 1;
            oi.UnitPrice = 125.00;
            oi.Tracking_Number__c = '12345';
            oi.Product_Name__c = 'STK-GF-109' ;
            orderItemList1.add(oi);
        }
		insert orderItemList1;  
    	//activate order
    	newOrder1.shipping_hold_status__c = ClsConstants.ORDER_STATUS_COMPLETED;
		update newOrder1;
        Test.stopTest();
		newOrder1.Status =ClsConstants.ORDER_STATUS_ACTIVATED;
		update newOrder1;
        
	}
    @isTest
    Static void closeOrder(){       
        Test.enableChangeDataCapture();
        Order ord =  [SELECT Id,Status FROM Order WHERE Status = 'Activated' LIMIT 1];
        Test.startTest();
        ord.Status =ClsConstants.ORDER_STATUS_CLOSED;
        update ord;
        Order updatedOrder = [Select id,status from Order where ID=:ord.id];
        System.assertEquals('CLOSED',updatedOrder.status);
        Test.stopTest();
    }
}