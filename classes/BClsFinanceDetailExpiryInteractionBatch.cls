global class BClsFinanceDetailExpiryInteractionBatch implements database.Batchable<SObject>  {
    global Database.QueryLocator start(Database.BatchableContext bc){
        Date lastDay= date.today().addMonths(1).toStartofMonth().addDays(-1);
          String SSIPQuery = 'SELECT Id, Account__c, Account__r.Id,Account__r.Number_of_Active_SSIP_Rules__c,  ' + 
               'Account__r.RecordType.DeveloperName ' + 
               'FROM Finance_Detail__c'
               +' WHERE (Account__r.RecordType.DeveloperName =\'GB_Consumer\' OR Account__r.RecordType.DeveloperName =\'IE_Consumer\' OR Account__r.RecordType.DeveloperName =\'CA_Consumer\')' +
               ' AND Account__r.Number_of_Active_SSIP_Rules__c > 0 AND Primary__c = True AND Expiry_Date_Converted__c=:lastDay';
        
        return Database.getQueryLocator(SSIPQuery);
    }
        global void execute(Database.BatchableContext bc, List<SObject> scope){
            Date sixDate= date.today();
            if(runningInASandbox()){
                sixdate=sixdate.addDays(-2);
            }
            else{
                sixdate=sixdate.addmonths(-6);
            }
            
            List<id> validDateId=new list<id>();
            map<id, Finance_Detail__c> idToDetail= new map<id,Finance_Detail__c>();
            
            for(Finance_Detail__c a : (List<Finance_Detail__c>)scope){
                validDateId.add(a.Account__r.Id);
                idToDetail.put(a.Account__r.Id, a);
            }
            for(marketing_interaction__c mi: [SELECT id, Account__c, CreatedDate FROM marketing_interaction__c WHERE account__c in :validDateId AND 
                                              (Communication_Type__c='CA - Credit Card Expiry' OR Communication_Type__c='UKIE - Credit Card Expiry') ]){
                if(mi.CreatedDate>sixDate){
                    if(idToDetail.containskey(mi.account__c)){
                        idToDetail.remove(mi.account__c);
                    }
                }
            }
            List<marketing_interaction__c> newMI = new list<marketing_interaction__c>();
            for(Finance_Detail__c a :idToDetail.values()){
                marketing_interaction__c mi = new marketing_interaction__c();
                mi.Account__c=a.Account__r.Id;
                mi.Source_Record_Id__c=a.id;
                mi.Finance_Detail__c=a.id;
                if(a.Account__r.RecordType.DeveloperName =='GB_Consumer'){
                    mi.Country__c='United Kingdom';
                    mi.Communication_Type__c='UKIE - Credit Card Expiry';
                }else if (a.Account__r.RecordType.DeveloperName =='IE_Consumer'){
                    mi.Country__c='Ireland';
                    mi.Communication_Type__c='UKIE - Credit Card Expiry';
                }else{
                    mi.Country__c='Canada';
                    mi.Communication_Type__c='CA - Credit Card Expiry';
                }
                newMI.add(mi);
            }
           insert newMI; 
        }
       public static Boolean runningInASandbox() {
           return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
       }
       
        global void finish(Database.BatchableContext bc){
        }
   }