global class ClsSampleOrderCalloutReqHandler  implements /*Queueable, */ Database.batchable<Sobject>, Database.AllowsCallouts {
   
    global String jsonOutEvtString;
    global List<Sample_Order_Response_Event__e> lstOCEevt;
    //public  Sample_Order_Response_Event__e OCESampleRequest;
    global ClsSampleOrderCalloutReqHandler(List<Sample_Order_Response_Event__e> lstOCEevt){
        System.debug('**OCE-ClsSampleOrderCalloutReqHandler--lstOCEevt---'+lstOCEevt.size());
        this.lstOCEevt = lstOCEevt;
    }
    global String createJson(Sample_Order_Response_Event__e evt) {
        String js = '{';
        if (String.isNotBlank(evt.OCEOrderReference__c)) js = jsonField(js, 'OCEOrderReference__c', evt.OCEOrderReference__c);
        if (String.isNotBlank(evt.OrderNumber__c)) js = jsonField(js+', ', 'OrderNumber__c', evt.OrderNumber__c);
        if (String.isNotBlank(evt.OrderStatus__c)) js = jsonField(js+', ', 'OrderStatus__c', evt.OrderStatus__c);
        if (String.isNotBlank(evt.TrackingUrl__c)) js = jsonField(js+', ', 'TrackingUrl__c', evt.TrackingUrl__c);
        if (String.isNotBlank(evt.IntegrationStatus__c)) js = jsonField(js+', ', 'IntegrationStatus__c', evt.IntegrationStatus__c);
        if (String.isNotBlank(evt.IntegrationErrorMessage__c)) js = jsonField(js+', ', 'IntegrationErrorMessage__c', evt.IntegrationErrorMessage__c);
        js = js + '}';
        return js;
    }
    global String jsonField(String inString, String field, String value) {
        return ( inString + '\"'+ field +'\":\"' + value + '\"');
    }
    global class ObjectIterator implements Iterator<Sobject>, Iterable<Sobject>{
        global List<Sobject> lstObject; Integer i = 0;
        global ObjectIterator(List<Sobject> lstObject){  this.lstObject = lstObject; i = 0; }  
        global Boolean hasNext(){ return (i >= lstObject.size()) ? false : true; } 
        global Sobject next(){ if(i < lstObject.size()){ i++; return lstObject[i-1]; } return null;}  
        global Iterator<Sobject> iterator() { return this;  } 
    }
    
    global Iterable<Sobject> start(Database.BatchableContext BC){
        System.debug('**OCE--start--');
        return new ObjectIterator(lstOCEevt);
    }
    global void execute(Database.BatchableContext BC, List<Sample_Order_Response_Event__e> lstOCEevt) {
        System.debug('**OCE--Execute--'+lstOCEevt[0]);
        String responsebody = '';
        String OCESampleRequest = createJson(lstOCEevt[0]);
        try{
            System.debug('**OCE--Request Body is-->' + OCESampleRequest);
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint('callout:OCEOrderInfo/services/data/v49.0/sobjects/OCE_DTP_INBOUND__e');
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Authorization', 'BASIC {!$Credential.AuthorizationHeaderValue}');
            request.setBody(OCESampleRequest);
            request.setTimeout(20000); 
            HttpResponse response = http.send(request);
            System.debug('**OCE-response======='+response);
            if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                responsebody =  response.getBody();
                ErrorUtility.LogErrorwithEvent('Publish', 'ClsSampleOrderCalloutReqHandler', 'Publishing response to OCE', 'order', 'crm', 'Published Order status with Response Code 200/201', '', 'Informational', OCESampleRequest);
            }else if(response.getStatusCode() == 302){
                System.debug('**OCE--Header--'+response.getHeader('Location'));
                ErrorUtility.LogError('Publish', 'ClsSampleOrderCalloutReqHandler', 'Publishing response to OCE', 'order', 'crm', 'Published Order status received response code 302. Will retry.', '', 'Informational');
                request.setEndpoint(response.getHeader('Location'));
                response = new Http().send(request); 
                if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                    responsebody =  response.getBody();
                    ErrorUtility.LogError('Publish', 'ClsSampleOrderCalloutReqHandler', 'Publishing response to OCE', 'order', 'crm', 'Published Order status after 302 and received Response Code 200.', '', 'Informational');
                } else {
                    ErrorUtility.LogErrorwithEvent('Publish', 'ClsSampleOrderCalloutReqHandler', 'Error in publishing response to OCE', 'order', 'crm', 'Received Response after 302 redirection: ' + response.getStatus(), '', 'Fatal Error - Bailing out', OCESampleRequest);
                }
            }else{
                ErrorUtility.LogErrorwithEvent('Publish', 'ClsSampleOrderCalloutReqHandler', 'Error in publishing response to OCE', 'order', 'crm', 'Received Response when performed a http post: ' + response.getStatus() + ', Response Code: '  + response.getStatusCode() + ', Response body: ' + response.getBody(), '', 'Fatal Error - Bailing out', OCESampleRequest);
                return;
            }
            
            
        }catch(Exception ex){
            System.debug('****OCE--ErrLine--'+ex.getLineNumber());
            System.debug('****OCE--ErrMsg--'+ex.getMessage());
            ErrorUtility.LogErrorwithEvent('Publish', 'ClsSampleOrderCalloutReqHandler', 'Error in publishing response to OCE', 'Order', 'crm', ex.getMessage(), '', 'Fatal Error - Bailing out', OCESampleRequest);
        }
    }
    global void finish(Database.BatchableContext BC){ }
}