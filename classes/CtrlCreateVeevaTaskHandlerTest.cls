/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created  : 11/01/2021
@Description   : Test class for CtrlCreateVeevaTaskHandler
****************************************************************************************************************/
@IsTest
public class CtrlCreateVeevaTaskHandlerTest {  
    @IsTest
    static void createTask(){
    Test.setMock(HttpCalloutMock.class, new ClsVeevaMockResponse());
    Task newTask = new Task();
            newTask.Subject='Test Subject';
            newTask.status='Open';
            newTask.priority='test priority';
            newTask.OwnerID= UserInfo.getUserId();
            newTask.Description ='test description';
            newTask.ActivityDate=system.today();
            newTask.Veeva_User_Id__c = '00002222222';
      test.startTest();
        CtrlCreateVeevaTaskHandler.createTask(newTask);
      test.stopTest();
    }  
  
}