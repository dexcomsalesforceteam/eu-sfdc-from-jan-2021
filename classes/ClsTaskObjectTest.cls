@istest
public class ClsTaskObjectTest {

  @istest
  static void testSecondRequestedOrderError(){
        Account ac=TestUtils.testDEConsumerAccount();
        ac.personBirthDate=Date.today()-500;
        insert ac;
        Task t=new Task();
        t.whatId=ac.id;
        t.Call_Reason__c='Second Sensor Requested';
        Test.startTest();
        insert t;
        Test.StopTest();
        
        
        }
     @istest
     static void testSecondRequestedOrder(){
     Map<String, String> mapPriceBook = ClsTestDataFactory.createCPB(new List<String>{'DE No Charge'},'EUR');
        Map<String, Id> mapProduct = ClsTestDataFactory.createProducts(new List<String>{'STS-GS-002','STT-GS-003'});
        Map<Id, Decimal> productIdToPriceMap = new Map<Id, Decimal>();
        for(Id productId : mapProduct.values()){
            productIdToPriceMap.put(productId, 10); 
        }
        Map<Id, Id> mapPBE = ClsTestDataFactory.createCustomPricebookEntries(productIdToPriceMap, mapPriceBook.get('DE No Charge'));
        Account ac=TestUtils.testDEConsumerAccount();
        ac.personBirthDate=Date.today()-500;
        insert ac;
        Task t=new Task();
        t.whatId=ac.id;
        t.Call_Reason__c='Second Sensor Requested';
        Test.startTest();
        insert t;
        Test.StopTest();
        
        
        }
       
     @istest
     static void testSecondRequestedOrder2(){
     Map<String, String> mapPriceBook = ClsTestDataFactory.createCPB(new List<String>{'DE No Charge'},'EUR');
        Map<String, Id> mapProduct = ClsTestDataFactory.createProducts(new List<String>{'STS-GS-002','STT-GS-003'});
        Map<Id, Decimal> productIdToPriceMap = new Map<Id, Decimal>();
        for(Id productId : mapProduct.values()){
            productIdToPriceMap.put(productId, 10); 
        }
        Map<Id, Id> mapPBE = ClsTestDataFactory.createCustomPricebookEntries(productIdToPriceMap, mapPriceBook.get('DE No Charge'));
        Account ac=TestUtils.testDEConsumerAccount();
        ac.personBirthDate=Date.today()-500;
        ac.Mktg_Journey_Touch_Points__c='Sample Order Sent';
        insert ac;
        Task t=new Task();
        t.whatId=ac.id;
        t.Call_Reason__c='Second Sensor Requested';
        Test.startTest();
        insert t;
        Test.StopTest();
        
        
        }
        
      @isTest
      static void testSecondRequestedOrderisPresent(){
       
      Map<String, String> mapPriceBook = ClsTestDataFactory.createCPB(new List<String>{'DE No Charge'},'EUR');
        Map<String, Id> mapProduct = ClsTestDataFactory.createProducts(new List<String>{'STS-GS-002','STT-GS-003'});
        Map<Id, Decimal> productIdToPriceMap = new Map<Id, Decimal>();
        for(Id productId : mapProduct.values()){
            productIdToPriceMap.put(productId, 10); 
        }
        Map<Id, Id> mapPBE = ClsTestDataFactory.createCustomPricebookEntries(productIdToPriceMap, mapPriceBook.get('DE No Charge'));
        Account ac=TestUtils.testDEConsumerAccount();
        ac.personBirthDate=Date.today()-500;
        ac.Mktg_Journey_Touch_Points__c='Sample Order Sent;Second Sensor Requested';

        insert ac;
        Task t=new Task();
        t.whatId=ac.id;
        t.Call_Reason__c='Second Sensor Requested';
        Test.startTest();
        insert t;
        Test.StopTest();
        
        
        }
}