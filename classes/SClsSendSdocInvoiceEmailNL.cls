/*****************************************************************************************
@Description    : Scheduler class for Benelux Email Invoice Automation
****************************************************************************************/
global class SClsSendSdocInvoiceEmailNL implements Schedulable {
    global void execute(SchedulableContext SC) {
        BClsSendSdocInvoiceEmailAllCountry sch = new BClsSendSdocInvoiceEmailAllCountry('NL');
        Database.executebatch(sch, 1);
    }
}