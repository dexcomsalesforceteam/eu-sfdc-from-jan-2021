/*******************************************************************************************
@Author        : Pravin jagadale
@Date Created  : 12/10/2020
@Description   : OrderItem Inventory Provider
*********************************************************************************************/
public class ClsItemInventoryProvider {

    @AuraEnabled
    public static List<PriceBookEntry> getPriceBookEntries(String OrderId){
        List<PriceBookEntry> lstPBE = new List<PriceBookEntry>();
        Order ord = [Select id,Warehouse__c,Pricebook2Id from Order where id=:OrderId]; 
        System.debug('ord.Pricebook2Id---'+ord.Pricebook2Id);
        if(ord.Pricebook2Id != null || ord.Pricebook2Id != ''){
            lstPBE = [Select Id, Name, Product2Id,Product2.Name,ProductCode,IsActive, PriceBook2Id, PriceBook2.Name 
                      From PriceBookEntry 
                      where PriceBook2Id=:ord.Pricebook2Id And IsActive=true];
        }
        
        if(lstPBE.size()>0) 
            return lstPBE;
        else
            return null; 
    }
     @AuraEnabled
    public static List<Resultwrapper> getAvailableQuantity(List<String> lstOfItems,String OrderId){
        List<Resultwrapper> lstResultWrapper = new List<Resultwrapper>();
        List<Items> lstitems = new List<Items>(); 
        Order ord = [Select id,Warehouse__c from Order where id=:OrderId];        
        for(Integer i=0;i<lstOfItems.size();i++){
            lstitems.add(new Items(lstOfItems[i],String.valueOf(ord.Warehouse__c)));
        }
        
        String requestBody = JSON.serialize(new RequestWrapper(lstitems));
        System.debug('requestBody--'+requestBody);
        try{
            
            
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint('callout:ItemInventoryCredentials');
            request.setMethod('POST');
            request.setHeader('Content-Type', ' application/json');
            request.setBody(requestBody);
            request.setTimeout(20000); 
            HttpResponse response = http.send(request);
            System.debug('======Status======='+response.getStatusCode()+'======body======='+response.getBody());
            if(response.getStatusCode() == 200){
                String responseBody = response.getBody();
                responseBody = responseBody.replace('items', 'itemsResult');            
                ClsItemInventoryProvider.ResponseWrapper ItemInventoryResponse = (ClsItemInventoryProvider.ResponseWrapper)JSON.deserialize(responseBody, ClsItemInventoryProvider.ResponseWrapper.class);
                for(ClsItemInventoryProvider.ItemsResult Items: ItemInventoryResponse.itemsResult){
                    lstResultWrapper.add(new Resultwrapper(Items.ItemNumber,Integer.valueof(Items.AvailableToTransact)));
                }
                return lstResultWrapper;  
            }else{
                //return 'ERROR';        
            }
        }catch(Exception ex){
            System.debug('--Error In HTTP Callout--'+ex.getMessage());
            System.debug('--Error Line In HTTP Callout--'+ex.getLineNumber());
            
        }
        return lstResultWrapper;
    }
    
    public class RequestWrapper{
        public  List<Items> items;
        public RequestWrapper(List<Items> items1){
             this.items = items1;
        }
    }    
    public class Items {
        public String ItemNumber;
        public String OrganizationCode;
        
        public Items( String ItemNumber1, String OrganizationCode1){
            ItemNumber = ItemNumber1;
            OrganizationCode = OrganizationCode1;
        }
    }   
    public class ResponseWrapper{
        public List<ItemsResult> itemsResult;
        
    }
    public class ItemsResult {
        public Integer AvailableToTransact;
		public String ItemNumber;
		/*public String ItemRevision;
		public String OrganizationCode;
		public String PrimaryUnitOfMeasure;
		public Integer QuantityOnhand;
		public String Subinventory;*/
       
    }    
    public class Resultwrapper{
        @AuraEnabled public String resItemNumber{get;set;}
        @AuraEnabled public Integer resAvailableToTransact{get;set;}        
        public Resultwrapper(String resItemNumber1,Integer resAvailableToTransact1){
            resItemNumber = resItemNumber1;
            resAvailableToTransact = resAvailableToTransact1;
        }        
    }
    
}