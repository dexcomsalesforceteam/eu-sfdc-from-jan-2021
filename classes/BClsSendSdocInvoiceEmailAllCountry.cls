/*
@Author        : Kumar Navneet
@Date Created    : 07/10/2020
@Description    : Batch class for Email Invoice Automation
****************************************************************************************/
global class BClsSendSdocInvoiceEmailAllCountry implements Database.Batchable<Sobject> {    
    private List<SDOC__SDJob__c> jobList = new List<SDOC__SDJob__c> {};
    private Map<String, String> mapETemId = new Map<String, String>(); 
    private String country;
    private List<Email_Invoice_Automation__mdt> InvList = new List<Email_Invoice_Automation__mdt>();
    private Boolean PODFlag = False ;
    private Map<String, ContentVersion> mapDocId = new Map<String, ContentVersion>(); 
    
    global BClsSendSdocInvoiceEmailAllCountry(String country){     
        String recordTypeName ;
        this.country = country;
        if(country == 'CLINICAL'){           
            recordTypeName = 'INTL SDoc Template';
        } else {
            //Get all the invoice templates that are required for processing
            recordTypeName = String.isNotBlank(country) && String.isNotEmpty(country) ? country + '%' : '';
        }     
        this.InvList = [SELECT Key_Field__c, Key_Field_Value__c, Invoice_Name__c, HTML_Template_Name__c, Country_Code__c FROM Email_Invoice_Automation__mdt where Country_Code__c = : country]; 
        String templateQuery = 'Select Id, Name from SDOC__SDTemplate__c Where SDOC__Document_Category__c = \'Invoice\'';
        templateQuery+= String.isNotBlank(recordTypeName) && String.isNotEmpty(recordTypeName) ? (' AND Recordtype.Name LIKE \'' + recordTypeName + '\'') : '';
        system.debug('Template Query is ' + templateQuery);
        for(SDOC__SDTemplate__c Sd : Database.query(templateQuery)){
            this.mapETemId.put(Sd.Name, Sd.Id);
        }
        String epod = '%Epod%';
        String DocQuery = 'Select Id, ContentDocumentId, Title from ContentVersion where Title LIKE \'' + epod + '\' and CreatedDate = LAST_N_DAYS:10   ' ;
        for(ContentVersion Cv : Database.query(DocQuery)){
            this.mapDocId.put(Cv.ContentDocumentId, Cv);
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC){        
        //If there is no country filter then query for all Orders that are to be processed for Email Invoicing
        String orderType = String.isNotBlank(country) && String.isNotEmpty(country) ? country + '%' : '';     
        String query = 'SELECT Id, Invoice_Number__c, Type ';
        system.debug('InvList === ' + InvList); 
        for(Email_Invoice_Automation__mdt Em :InvList) {
            system.debug('Em.Key_Field__c = ' + Em.Key_Field__c);
            query+= String.isNotBlank(Em.Key_Field__c) && String.isNotEmpty(Em.Key_Field__c) ? (' , ' + Em.Key_Field__c + '') : '';
            if(Em.Key_Field__c.contains('POD')) {
                this.PODFlag = True ;
                query+= ' , (Select LinkedEntityId, ContentDocumentId from ContentDocumentLinks)  ' ; 
            }
            break;
        }          
        query+= ' FROM Order where Invoice_Email_Status__c = \'Pending\'' ;                  
        query+= ' AND Email_Invoice_Opt_In__c = true ' ; 
        query+= String.isNotBlank(orderType) && String.isNotEmpty(orderType) ? (' AND Type LIKE \'' + orderType + '\'') : '';
        if(PODFlag)
            query+= ' AND POD_Received__c = True ' ; 
        system.debug('Start Query is' + query);                
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Order> scope){ 
        for(Order objOrder : scope){  
            String strPDFTemplateId ;
            String strHTMLTemplateId ;
            String strePodId ;
            String KeyFieldVar ='' ;          
            if(objOrder.Invoice_Number__c != null && objOrder.Invoice_Number__c != ''){ 
                for(Email_Invoice_Automation__mdt Em :InvList) {
                    if(objOrder.Type.substring(0,2) == Em.Country_Code__c){
                        system.debug('Em.Key_Field__c = ' + Em.Key_Field__c);   
                        if(Em.Key_Field__c.contains('RecordType')) {
                            if(Em.Key_Field__c.contains('Payor')) {
                                KeyFieldVar = objOrder.Payor__r.RecordType.name == null ? objOrder.Account.RecordType.Name : objOrder.Payor__r.RecordType.name;
                            } else {
                                KeyFieldVar = objOrder.Account.RecordType.Name ;
                            }
                            
                        } else {
                            if(Em.Key_Field__c.contains('Preferred_Language__c')){
                                KeyFieldVar = objOrder.Account.Preferred_Language__c ;
                            } else {
                                if(Em.Key_Field__c.contains('Billingcountry') || Em.Key_Field__c.contains('Warehouse__c') ){
                                    KeyFieldVar = objOrder.Payor__r.Billingcountry == null ? objOrder.Account.Billingcountry : objOrder.Payor__r.Billingcountry != null ? objOrder.Payor__r.Billingcountry : objOrder.Warehouse__c ;
                                }                                                
                            } 
                        } 
                        if(KeyFieldVar != null && KeyFieldVar != '' && Em.Key_Field_Value__c != null && Em.Key_Field_Value__c.contains(KeyFieldVar) ){                                                        
                            strPDFTemplateId = mapETemId.get(Em.Invoice_Name__c) ;
                            strHTMLTemplateId = mapETemId.get(Em.HTML_Template_Name__c) ; 
                        }
                    } 
                }          
                
                for (ContentDocumentLink Atc : objOrder.ContentDocumentLinks){   
                    if(mapDocId.get(Atc.ContentDocumentId) != null)
                    { 
                        strePodId = mapDocId.get(Atc.ContentDocumentId).ContentDocumentId ;
                        system.debug('strePodId'+ strePodId);
                    }                     
                }              
                List<String> lstTemplateIds = new List<String>();
                List<String> lstTemplateAttachIds = new List<String>();
                if(String.isNotEmpty(strePodId) && String.isNotBlank(strePodId)) lstTemplateAttachIds.add(strePodId);
                if(String.isNotEmpty(strPDFTemplateId) && String.isNotBlank(strPDFTemplateId)) lstTemplateIds.add(strPDFTemplateId);
                if(String.isNotEmpty(strHTMLTemplateId) && String.isNotBlank(strHTMLTemplateId)) lstTemplateIds.add(strHTMLTemplateId);
                
                system.debug('lstTemplateIds' +lstTemplateIds);
                if(!lstTemplateIds.isEmpty()){                
                    jobList.add(new SDOC__SDJob__c(SDOC__Start__c = true,  SDOC__Oid__c = objOrder.Id, SDOC__ObjApiName__c = 'Order', SDOC__SendEmail__c = '1', SDOC__Fid__c = String.join(lstTemplateAttachIds, ',') , SDOC__Doclist__c = String.join(lstTemplateIds, ','))); 
                }    
                update new Order(Id = objOrder.Id, Invoice_Email_Status__c = 'Sent'); 
            }  
        }        
        if(!jobList.isEmpty())insert jobList;
        update scope;
    }
    global void finish(Database.BatchableContext BC){
        system.debug('Order Finish: ');    }
}