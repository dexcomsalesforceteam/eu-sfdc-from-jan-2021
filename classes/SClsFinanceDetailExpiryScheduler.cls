global class SClsFinanceDetailExpiryScheduler implements Schedulable{
	global void execute(SchedulableContext SC) {
        Database.executeBatch(new BClsFinanceDetailExpiryInteractionBatch(), 100);
    }
}