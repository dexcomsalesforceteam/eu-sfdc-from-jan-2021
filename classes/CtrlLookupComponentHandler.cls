/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created  : 08/01/2021
@Description   : Handler class for lightning component LtngCustomLookup component
****************************************************************************************************************/
global class CtrlLookupComponentHandler{
    //callout to get field sales Users from Veeva
	@Auraenabled(cacheable=true)
    public static List<responseTaskWrapper> getVeevaUsers(String searchKeyWord,String ObjectName){
    	List<responseTaskWrapper> returnList = new List<responseTaskWrapper>();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:VeevaNamedCredentials/services/apexrest/SfdcTaskWebServices/getUsers');	
        request.setMethod('GET');
        try{
         	HttpResponse response = http.send(request);
            System.debug('@@@@@@@@ rResponse @@@@@@@@'+ response.getBody());
            List<responseTaskWrapper> vUsers = (List<responseTaskWrapper>)JSON.deserialize(response.getBody(), List<responseTaskWrapper>.class);
            if(searchKeyWord =='all'){
            	returnList=vUsers;
			}else{
        		for(responseTaskWrapper responseKeywords:vUsers){
            		if(responseKeywords.Name.containsIgnoreCase(searchKeyWord)){
                		returnList.add(responseKeywords);
            		}
				} 
        	}
		}catch(System.CalloutException e){
        	System.debug('Callout error: '+ e);
        } 
        return returnList;
	}
    //wrapper for response
    Public class responseTaskWrapper{
    @Auraenabled
    public String Name{get;set;}
    @Auraenabled
    public String Id{get;set;}  
	}
}