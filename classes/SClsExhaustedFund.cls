Global class SClsExhaustedFund implements Schedulable{
    global void execute(SchedulableContext sc){
        Id BatchProcessId = Database.ExecuteBatch(new BClsHandleExhaustedFund());
        System.debug('**SClsExhaustedFund--'+BatchProcessId);
    }
}