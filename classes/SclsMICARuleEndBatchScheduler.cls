global class SclsMICARuleEndBatchScheduler implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new BclsMarketingInteractionCARuleEndBatch(), 10);
    }
}