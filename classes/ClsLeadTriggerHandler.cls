/********************************************************
**Description: Lead trigger handler Class. All the CRUD events are handled here.
**Author:      Sundog
**Date Created:    JUNE.12.2019
*******************************************************/
public class ClsLeadTriggerHandler {
    
    /*********************************************************
  **@Author Sundog
  **@Date: 6/12/2019
  **@Reason Add Email Double Opt-In logic
  *********************************************************/
    public static void doubleOptInBefore(List<Lead> leads, Map<Id, Lead> oldMap){
      Set<Id>recordTypeIds = ClsMarketingCloudHelperEmail.getValidLeadRecordTypeIds();
        if (ClsMarketingCloudHelperEmail.RUN_TRIGGER_LOGIC) {
            for(Lead l : leads){
                if (recordTypeIds.contains(l.RecordTypeId)
                    && l.Email != oldMap.get(l.Id).Email
                    && l.Email_Double_opt_In_Subscriber__c != null) {
                    l.Email_Pending_Opt_In_Reset__c = true;
                }
            }
           
        for(Lead l : leads){
        if (l.Email_Pending_Opt_In_Reset__c) {
                    l.Email_Double_Opt_In_Subscriber__c = null;
                    l.Email_Pending_Opt_In__c = true;
                }
            }
        }
    }
    
    /*********************************************************
  **@Author Sundog
  **@Date: 6/12/2019
  **@Reason Add Email Double Opt-In logic
  *********************************************************/
  public static void doubleOptInAfter(List<Lead> leads, Map<Id, Lead> oldMap){

      
        //because this org converts leads immediate, do not push into marketing cloud as a lead
        if(leads.size() == 1){
            Set<Id> recordTypeIds = ClsMarketingCloudHelperEmail.getValidLeadRecordTypeIds();
                for (Lead l : leads){
                    // don't send doubleoptin for leads that qualify for conversion - it will send for the account after it is converted
                    if(l.Sample_Program_Lead_Type__c=='DTC' && l.LeadSource=='$Hello Dexcom' && l.status=='Qualified'){
                        continue;
                    }
                    if(l.Email != null && recordTypeIds.contains(l.RecordTypeId)) {
                        if (l.Email_Double_Opt_In_Subscriber__c == null
                            && ((l.Email_Pending_Opt_In__c && (oldMap == null || !oldMap.get(l.Id).Email_Pending_Opt_In__c))
                            || (l.Email_Pending_Opt_In_Reset__c && (oldMap == null || !oldMap.get(l.Id).Email_Pending_Opt_In_Reset__c)))) {
                                ClsMarketingCloudHelperEmail.sendDoubleOptInFuture(l.Id);
                        }
                    }
            }
        }
    }
    
     public static void afterInsert(List<Lead> triggerNew){
      convertDTCLeadToAccount(triggerNew);
     }
     
     public static void afterUpdate(List<Lead> triggerNew){
      List<Lead> eligibleLeadListToConvert=new List<Lead>();
       for(Lead l: triggerNew){
          if(l.Sample_Program_Lead_Type__c=='DTC' && l.LeadSource=='$Hello Dexcom' && l.status=='Qualified') eligibleLeadListToConvert.add(l);
       }
       if(eligibleLeadListToConvert.size()>0) convertDTCLeadToAccount(eligibleLeadListToConvert);
     
     }
    
    //NS-1740 when $HelloDexcom lead creating, it converts to person account
    public static void convertDTCLeadToAccount(List<Lead> triggerNew){
      String sampleOrder='Sample Order Sent';
      List<String> leadFirstName=new List<String>();
      List<String> leadLastName=new List<String>();
      List<Date> listDOB=new List<Date>();
      List<String> listEmail=new List<String>();
      Id sfAdminId = [SELECT Id from user where name = 'Salesforce Admin'].id;
      try{
      for(Lead l: triggerNew){
        if(l.Sample_Program_Lead_Type__c=='DTC' && l.LeadSource=='$Hello Dexcom'){
        
         if(l.lastname != null && l.Birth_Date__c != null){
         leadFirstName.add(l.firstname);
         leadLastName.add(l.lastname);
         listDOB.add(l.Birth_Date__c);
         }
         if(l.email != null){
         listEmail.add(l.email);
         }  
       }         
      }
      Map<string,Account> actMapWithName=new Map<String,Account>();
      Map<string,account> actMapWithEmail=new Map<String,account>();
      if((leadLastName.size()>0 && listDOB.size()>0) || listEmail.size()>0){          
        List<String> esitingActEmail=new List<String>();    
       
        for(Account act: [select id,name,firstname,lastname,PersonEmail,personBirthDate,billingcountrycode from Account where  (personBirthDate IN:listDOB AND firstname IN: leadFirstName and lastname IN:leadLastName) OR PersonEmail IN:listEmail]){
          if(act.lastname !=null && act.personBirthDate != null){
            String key=act.billingcountrycode+'_'+act.firstname+'_'+act.lastname+'_'+act.personBirthDate;          
            actMapWithName.put(key,act);         
          }
          if(act.PersonEmail !=null){         
          actMapWithEmail.put(act.PersonEmail,act);
          }
        }
        
      }
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true limit 1];
        List<Database.LeadConvert> convertLeadList=new List<Database.LeadConvert>();
        List<Task> insertTask=new List<Task>();
        Map<id,string> leadIdToCountryCodeMap=new Map<id,string>();
        Map<Id,String> leadIdToBGMeasurementType = new Map<Id,String>();
        for(Lead lead : triggerNew){
          if(lead.Sample_Program_Lead_Type__c=='DTC' && lead.LeadSource=='$Hello Dexcom'){
              leadIdToCountryCodeMap.put(lead.id,lead.CountryCode);
              if (lead.Unit_Of_Measurement__c == 'mg/dl') leadIdToBGMeasurementType.put(lead.Id, 'mgdl ');
              else if (lead.Unit_Of_Measurement__c == 'mmol/l') leadIdToBGMeasurementType.put(lead.Id, 'mmoll ');
              else leadIdToBGMeasurementType.put(lead.Id, '');
              String key=lead.countrycode+'_'+lead.firstName+'_'+lead.lastName+'_'+lead.Birth_Date__c;            
              if(actMapWithName.containsKey(key) || actMapWithEmail.containskey(lead.email)){
                Account duplicateAct=actMapWithName.get(key)!=null?actMapWithName.get(key):actMapWithEmail.get(lead.email);             
                //create task saying that  duplicate accout found
                 String commnet='With the same details we found an account :'+duplicateAct.name+' , Account ID: '+duplicateAct.id;               
                insertTask.add(createTask(null,lead.id,'Duplciate Account found: '+duplicateAct.name,lead.CountryCode,commnet));                
              }else{               
                  Database.LeadConvert lc = new Database.LeadConvert();
                      lc.setLeadId(lead.Id);
                      lc.setConvertedStatus(convertStatus.MasterLabel);
                      lc.setDoNotCreateOpportunity(true);
                  	  lc.setOwnerId(sfAdminId);
                      convertLeadList.add(lc);
              }
         }
        }
        
        //convert the leads to account then first sample order creation will be called
        if(convertLeadList.size()>0){
          List<Database.LeadConvertResult> result=Database.convertLead(convertLeadList);
          Set<id> actId=new Set<id>();
            system.debug(result.size());
          for(Database.LeadConvertResult re: result){
              
            Id leadId=re.getLeadId();
            if(re.isSuccess()){               
               Id newActId=re.getAccountId();                      
               Map<string,string> orderresult=ClsSampleProgramHandler.createOrder(newActId, leadIdToCountryCodeMap.get(leadId)+' '+ leadIdToBGMeasurementType.get(leadId) + sampleOrder,null,0,null);
               if(orderresult.containsKey('Success')){
                   actId.add(newActId);                  
              }else{                  
                 ErrorUtility.LogError('Consume', 'ClsLeadTriggerHandler.afterinsert', orderresult.get('Failed'), 'First Sample Order creation failed', 'crm', '', 'Account ID: ' + newActId,'Fatal Error - Bailing out');
                 //create task if first order alrady present                        
                 insertTask.add(createTask(newActId,null,'First Sales Order Failed To Create',leadIdToCountryCodeMap.get(leadId),orderresult.get('Failed')));                              
               }
            }else{
             //if lead conversion failed then create task and log
              insertTask.add(createTask(null,leadId,'Lead conversion failed',leadIdToCountryCodeMap.get(leadId),re.getErrors()[0].getMessage())); 
              ErrorUtility.LogError('Consume', 'ClsLeadTriggerHandler.afterinsert', re.getErrors()[0].getMessage(), 'ClsLeadTriggerHandler', 'crm', '', 'Lead ID: ' + leadId,'Fatal Error - Bailing out');           
            }
          }
          if(actId.size()>0){
            List<Account> updateAccountList=new List<Account>();
            for(Account acct: [select id,Mktg_Journey_Touch_Points__c from account where id IN :actId ]){
               acct.Mktg_Journey_Touch_Points__c=acct.Mktg_Journey_Touch_Points__c !=null?acct.Mktg_Journey_Touch_Points__c+';'+sampleOrder:sampleOrder;
               updateAccountList.add(acct);
            }
            if(updateAccountList.size()>0) update updateAccountList;
          }
        }
        if(insertTask.size()>0) insert insertTask; 
        
     }catch(Exception e){
       ErrorUtility.LogError('Consume', 'ClsLeadTriggerHandler.afterinsert', e.getMessage()+'_'+e.getLineNumber(), 'ClsLeadTriggerHandler', 'crm', '', '','Fatal Error - Bailing out');  
       system.debug('Error Line'+e.getLineNumber());
       system.debug('Error Message'+e.getMessage());
     }
    }
    
    public static Task createTask(Id whatId,Id whoId,string subject,string countrycode,string commnets){
        Id devRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(countrycode+' Task').getRecordTypeId();
        Task tsk = new Task();
        tsk.Subject = subject;        
        tsk.whoId =whoId!=null?whoId:null;
        tsk.whatId=whatId!=null?whatId:null;
        tsk.Type = 'Other';
        tsk.recordtypeId=devRecordTypeId;
        tsk.Description=commnets;
        return tsk;
        
    }
    

}