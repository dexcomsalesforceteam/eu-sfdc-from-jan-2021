/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created  : 14/01/2021
@Description   : Mock response
****************************************************************************************************************/
@isTest
global class ClsVeevaMockResponse implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
         HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('[{"attributes":{"type":"User","url":"/services/data/v50.0/sobjects/User/0051t000001FYBVAA4"},"Id":"0051t000001FYBVAA4","Name":"Mavens Admin"},{"attributes":{"type":"User","url":"/services/data/v50.0/sobjects/User/0051t000001Qt6lAAC"},"Id":"0051t000001Qt6lAAC","Name":"Veeva Admin"}]');
        res.setStatusCode(200);
        return res;
    }
}