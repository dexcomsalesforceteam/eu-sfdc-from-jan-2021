global class SClsDEFutureOrderScheduler implements Schedulable {
    global void execute(SchedulableContext SC) {
        BClsFutureOrderHandler sch = new BClsFutureOrderHandler(Date.Today(), Date.Today(),'DE',null,null);
        Database.executebatch(sch, 1);
    }
}