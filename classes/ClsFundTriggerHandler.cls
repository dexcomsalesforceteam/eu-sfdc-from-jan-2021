public class ClsFundTriggerHandler {
    
    public void onBeforeInsert(List<Fund__c> lstFund){
        //Set the Currency Code on the Fund upon insert
        Set<Id> setAccountId = new Set<Id>();
        for(Fund__c fund : lstFund) setAccountId.add(fund.Payor__c);
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, RecordType.DeveloperName,country__c FROM Account WHERE Id IN: setAccountId]);
        for(Fund__c fund : lstFund){
            String recordTypeDName = mapAccount.get(fund.Payor__c).RecordType.DeveloperName;
            String currencyCode = recordTypeDName.startsWithIgnoreCase('CA_') ? 'CAD' : recordTypeDName.startsWithIgnoreCase('CH_') ? 'CHF' : 
            recordTypeDName.startsWithIgnoreCase('GB_') ? 'GBP' : recordTypeDName.startsWithIgnoreCase('IE_') ? 'EUR' : 
            recordTypeDName.startsWithIgnoreCase('INTL_') ? 'USD' : 'EUR';
            if(String.isNotBlank(currencyCode) && String.isNotEmpty(currencyCode)) fund.CurrencyIsoCode = currencyCode;
            
            // NS - 1579 - Code Added By Pravin
            if(fund.Country__c != mapAccount.get(fund.Payor__c).country__c && fund.Country__c == 'United Kingdom' ){
                fund.addError('Payor Should be from same country as Fund');
            }
            // NS - 1579 - End
            
        }
       
        checkDuplicateActiveFund(lstFund);
        
    }
    
    public void onBeforeUpdate(List<Fund__C> newList,Map<id,Fund__C> oldMap){
        Map<String,Account> mapOfPayorCountry = new Map<String,Account>();
        List<Fund__c> lstFund = new List<Fund__c>();
        Set<Id> setOfPayor = new Set<Id>();
        Id recordTypeId = Schema.SObjectType.Fund__c.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        for(Fund__C fund:newList){ 
            setOfPayor.add(fund.Payor__c);
            if(fund.Is_Fund_Active__c && fund.Succeeding_Fund__c !=null && fund.recordtypeid==recordTypeId &&  fund.Succeeding_Fund__c != oldMap.get(fund.id).Succeeding_Fund__c ){
                fund.Auth_End_Date__c = Date.today().adddays(-1);
                lstFund.add(fund);
            }
        }
        
        // NS - 1579 - Code Added By Pravin
        for(Account acc : [Select id,country__c,RecordType.DeveloperName from Account where Id IN:setOfPayor]){
            mapOfPayorCountry.put(acc.Id,acc);
        }
        for(Fund__c fund:newList){             
            if(mapOfPayorCountry.containsKey(fund.Payor__c) && fund.Country__c == 'United Kingdom'){
                if(fund.Country__c != mapOfPayorCountry.get(fund.Payor__c).Country__c){
                    fund.addError('Payor Should be from same country as Fund');
                }
            }	
        }
        // NS - 1579 - End
        checkDuplicateActiveFund(newList);
    }
    
    public void onAfterUpdate(List<Fund__C> newList,Map<id,Fund__C> oldMap){
       
        Id recordTypeId = Schema.SObjectType.Fund__c.getRecordTypeInfosByDeveloperName().get('Purchase_Order').getRecordTypeId();
        Map<Id,Id> mapOfFundToSuccFund=new Map<Id,Id>();
        //Prepare a map between the Fund and Succeeding fund to be passed on to the Batch class
        for(Fund__C fund:newList){  
            system.debug('fund.Succeeding_Fund__c'+fund.Succeeding_Fund__c+'>>'+fund.recordtypeid+'<>'+recordTypeId);
            if(fund.Succeeding_Fund__c !=null && fund.recordtypeid==recordTypeId && fund.Succeeding_Fund__c != oldMap.get(fund.id).Succeeding_Fund__c){               
                mapOfFundToSuccFund.put(fund.id,fund.Succeeding_Fund__c);
            }
        }
       
        if(!mapOfFundToSuccFund.isEmpty())
            database.executeBatch(new BclsHandleAccountFundBatch(mapOfFundToSuccFund),10);
        
        
    }
        
    //Check if any duplciate active fund
    public static void checkDuplicateActiveFund(List<Fund__C> newList){
        
        System.debug('checkDuplicateActiveFund Method--');
        Map<String,Fund__C> fundMap=new Map<String,Fund__C>();
        List<String> poNumList=new List<String>();
        
        for(Fund__c fund : newList){            
            if(fund.is_fund_active__C && fund.PO_Reference_Number__c!=null){
                poNumList.add(fund.PO_Reference_Number__c);
                String key=fund.Payor__c+'-'+fund.PO_Reference_Number__c;
                if(fundMap.containsKey(key)){
                    fund.addError('Duplicate Fund with same PO Number in batch');
                }else fundMap.put(key,fund);           
            }
        }
            
        if(!fundMap.isEmpty()){
            for(Fund__C fund:[select id,name,Payor__c,PO_Reference_Number__c from Fund__C where is_fund_active__C=true and PO_Reference_Number__c in : poNumList]){
                String key=fund.Payor__c+'-'+fund.PO_Reference_Number__c;
                if(fundMap.containsKey(key) && fund.id != fundMap.get(key).id){
                    fundMap.get(key).addError('Duplicate Fund found '+fund.name);
                }
            }
        }
        
        
    }
}