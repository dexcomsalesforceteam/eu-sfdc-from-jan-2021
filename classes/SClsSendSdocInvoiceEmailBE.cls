/*****************************************************************************************
@Description    : Scheduler class for Benelux Email Invoice Automation
****************************************************************************************/
global class SClsSendSdocInvoiceEmailBE implements Schedulable {
    global void execute(SchedulableContext SC) {
        BClsSendSdocInvoiceEmailAllCountry sch = new BClsSendSdocInvoiceEmailAllCountry('BE');
        Database.executebatch(sch, 1);
    }
}