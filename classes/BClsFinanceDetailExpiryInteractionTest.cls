/**
 * BClsFinanceDetailExpiryInteractionTest
 * test class for BClsFinanceDetailExpiryInteractionBatch
 * @author Anthony Thompson (Perficient)
 * @date 2020.01.04
 */

@isTest
public class BClsFinanceDetailExpiryInteractionTest {

    @IsTest
    static void CAConsumerWithExpiry(){
        // create account
        Account acc = TestUtils.testCAConsumerAccount();
        acc.Number_of_Active_SSIP_Rules__c = 1;
        insert acc;

        // create finance detail associated - expiry_date__c set to 'MM/YYYY'
        Finance_Detail__c fin = new Finance_Detail__c();
        DateTime now = DateTime.now();
        fin.expiry_date__c = now.format('MM/YYYY');
        fin.account__c = acc.id;
        fin.primary__c = true;
        insert fin;

        // run batch job
        Test.startTest();
        BClsFinanceDetailExpiryInteractionBatch monthlyExpiryBatch = new BClsFinanceDetailExpiryInteractionBatch();
        Id batchInstanceId = Database.executeBatch(monthlyExpiryBatch, 100);
        Test.stopTest();

        // check for marketing interaction
        Marketing_Interaction__c mi = [select account__c, finance_detail__c, country__c, communication_type__c from marketing_interaction__c];

        system.assertEquals('Canada', mi.country__c);
        system.assertEquals(acc.Id, mi.account__c);
        system.assertEquals(fin.id, mi.finance_detail__c);
        system.assertEquals('CA - Credit Card Expiry', mi.communication_type__c);
    }
    @IsTest
    static void GBConsumerWithExpiry(){
        // create account
        Account acc = TestUtils.testGBConsumerAccount();
        acc.Number_of_Active_SSIP_Rules__c = 1;
        insert acc;

        // create finance detail associated - expiry_date__c set to 'MM/YYYY'
        Finance_Detail__c fin = new Finance_Detail__c();
        DateTime now = DateTime.now();
        fin.expiry_date__c = now.format('MM/YYYY');
        fin.account__c = acc.id;
        fin.primary__c = true;
        insert fin;

        // run batch job
        Test.startTest();
        BClsFinanceDetailExpiryInteractionBatch monthlyExpiryBatch = new BClsFinanceDetailExpiryInteractionBatch();
        Id batchInstanceId = Database.executeBatch(monthlyExpiryBatch, 100);
        Test.stopTest();

        // check for marketing interaction
        Marketing_Interaction__c mi = [select account__c, finance_detail__c, country__c, communication_type__c from marketing_interaction__c];

        system.assertEquals('United Kingdom', mi.country__c);
        system.assertEquals(acc.Id, mi.account__c);
        system.assertEquals(fin.id, mi.finance_detail__c);
        system.assertEquals('UKIE - Credit Card Expiry', mi.communication_type__c);
    }
    @IsTest
    static void IEConsumerWithExpiry(){
        // create account
        Account acc = TestUtils.testIEConsumerAccount();
        acc.Number_of_Active_SSIP_Rules__c = 1;
        insert acc;

        // create finance detail associated - expiry_date__c set to 'MM/YYYY'
        Finance_Detail__c fin = new Finance_Detail__c();
        DateTime now = DateTime.now();
        fin.expiry_date__c = now.format('MM/YYYY');
        fin.account__c = acc.id;
        fin.primary__c = true;
        insert fin;

        // run batch job
        Test.startTest();
        BClsFinanceDetailExpiryInteractionBatch monthlyExpiryBatch = new BClsFinanceDetailExpiryInteractionBatch();
        Id batchInstanceId = Database.executeBatch(monthlyExpiryBatch, 100);
        Test.stopTest();

        // check for marketing interaction
        Marketing_Interaction__c mi = [select account__c, finance_detail__c, country__c, communication_type__c from marketing_interaction__c];

        system.assertEquals('Ireland', mi.country__c);
        system.assertEquals(acc.Id, mi.account__c);
        system.assertEquals(fin.id, mi.finance_detail__c);
        system.assertEquals('UKIE - Credit Card Expiry', mi.communication_type__c);
    }
    
	@IsTest
    static void CAConsumerPreventDuplicate(){
        // create account
        Account acc = TestUtils.testCAConsumerAccount();
        acc.Number_of_Active_SSIP_Rules__c = 1;
        insert acc;

        // create finance detail associated - expiry_date__c set to 'MM/YYYY'
        Finance_Detail__c fin = new Finance_Detail__c();
        DateTime now = DateTime.now();
        fin.expiry_date__c = now.format('MM/YYYY');
        fin.account__c = acc.id;
        fin.primary__c = true;
        insert fin;

        marketing_interaction__c mi = new marketing_interaction__c();
        mi.Account__c=acc.Id;
        mi.Finance_Detail__c=fin.id;
        mi.Country__c='Canada';
        mi.Communication_Type__c='CA - Credit Card Expiry';
        insert mi;

        // run batch job
        Test.startTest();
        BClsFinanceDetailExpiryInteractionBatch monthlyExpiryBatch = new BClsFinanceDetailExpiryInteractionBatch();
        Id batchInstanceId = Database.executeBatch(monthlyExpiryBatch, 100);
        Test.stopTest();

        // check for marketing interaction
        List<Marketing_Interaction__c> MIs = [select id from marketing_interaction__c];

		system.assertEquals(1, MIs.size());
    }
}