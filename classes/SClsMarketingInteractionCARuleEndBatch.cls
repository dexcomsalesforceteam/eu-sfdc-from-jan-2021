global class SClsMarketingInteractionCARuleEndBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        BclsMarketingInteractionCARuleEndBatch caRuleEndBatch = new BclsMarketingInteractionCARuleEndBatch();
        Database.executebatch(caRuleEndBatch, 20);
    }
    /*SClsMarketingInteractionCARuleEndBatch caMICARuleEndBatch = new SClsMarketingInteractionCARuleEndBatch();
    // Schedule to run at 1PM European timezone, which is 6AM Canadian time zone
    String sch = '0 0 13  * * ? *';
    String jobID = System.schedule('CARuleEndBatch6AMRun', sch, caMICARuleEndBatch);*/
}