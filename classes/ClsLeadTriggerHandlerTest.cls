@isTest
private class ClsLeadTriggerHandlerTest {
    
    @isTest
    static void testDoubleOptInResetDE(){
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        Lead testLead = TestUtils.testDEPersonLead();
        testLead.Email_Opt_In_Language__c = 'German';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testLead;
        
        //email address change
        testLead.Email='Test2@gmail.com';
        
        Test.startTest();
        update testLead;
        Lead resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultLead.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultLead.Email_Pending_Opt_In_Reset__c);
    }

    @isTest
    static void testDoubleOptInResetCA(){
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        Lead testLead = TestUtils.testCAPersonLead();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testLead;
        
        //email address change
        testLead.Email='Test2@gmail.com';
        
        Test.startTest();
        update testLead;
        Lead resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultLead.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultLead.Email_Pending_Opt_In_Reset__c);
    }
      @isTest
    static void testDoubleOptInResetGB(){
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        Lead testLead = TestUtils.testGBPersonLead();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testLead;
        
        //email address change
        testLead.Email='Test2@gmail.com';
        
        Test.startTest();
        update testLead;
        Lead resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultLead.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultLead.Email_Pending_Opt_In_Reset__c);
    }
     @isTest
    static void testDoubleOptInResetIE(){
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        Lead testLead = TestUtils.testIEPersonLead();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testLead;
        
        //email address change
        testLead.Email='Test2@gmail.com';
        
        Test.startTest();
        update testLead;
        Lead resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultLead.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultLead.Email_Pending_Opt_In_Reset__c);
    }
    
    
    @isTest
    static void testDoubleOptInResetAT(){
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        Lead testLead = TestUtils.testATPersonLead();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testLead;
        
        //email address change
        testLead.Email='Test2@gmail.com';
        
        Test.startTest();
        update testLead;
        Lead resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultLead.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultLead.Email_Pending_Opt_In_Reset__c);
    }
     @isTest
    static void testDoubleOptInResetCH(){
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        Lead testLead = TestUtils.testCHPersonLead();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testLead;
        
        //email address change
        testLead.Email='Test2@gmail.com';
        
        Test.startTest();
        update testLead;
        Lead resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultLead.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultLead=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Lead];
        System.assertEquals(true,resultLead.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultLead.Email_Pending_Opt_In_Reset__c);
    }
    
         @isTest
    static void testHelloDexcomError(){
        
        Lead testLead = new Lead();
        testLead.Phone = '123456789';
        testLead.City = 'Test City';
        testLead.Email = 'Test234565@gmail.com';
        testLead.Street = 'Test Street';
        testLead.PostalCode = '12345';
        testLead.Country = 'Germany';        
       
        testLead.Sample_Program_Lead_Type__c='DTC';
        testLead.LeadSource='$Hello Dexcom';       
        
        testLead.firstname='firstname';
        testLead.lastname='lastname';
        testLead.Email='Test2@gmail.com';
        testLead.Birth_Date__c=Date.today()-500;
        
        Test.startTest();
          insert testLead;
       test.stoptest();
    }
    @isTest
     static void testHelloDexcom(){
        Map<String, String> mapPriceBook = ClsTestDataFactory.createCPB(new List<String>{'DE No Charge'},'EUR');
        Map<String, Id> mapProduct = ClsTestDataFactory.createProducts(new List<String>{'STS-GS-002','STT-GS-003'});
        Map<Id, Decimal> productIdToPriceMap = new Map<Id, Decimal>();
        for(Id productId : mapProduct.values()){
            productIdToPriceMap.put(productId, 10); 
        }
        Map<Id, Id> mapPBE = ClsTestDataFactory.createCustomPricebookEntries(productIdToPriceMap, mapPriceBook.get('DE No Charge'));
        system.debug('>>>>>>>>'+[select id,name from pricebook2]);
        system.debug('>>>>>>>>2'+[select id,name from pricebook2 where name='DE No Charge']);
        Lead testLead = new Lead();
        testLead.Phone = '123456789';
        testLead.City = 'Test City';
        testLead.Email = 'Test234565@gmail.com';
        testLead.Street = 'Test Street';
        testLead.PostalCode = '12345';
        testLead.Country = 'Germany';        
       
        testLead.Sample_Program_Lead_Type__c='DTC';
        testLead.LeadSource='$Hello Dexcom';       
        
        testLead.firstname='firstname';
        testLead.lastname='lastname';
        testLead.Email='Test2@gmail.com';
        testLead.Birth_Date__c=Date.today()-500;
        
        Test.startTest();
          insert testLead;
       test.stoptest();
    }
         @isTest
    static void testHelloDexcomDuplicateCheck(){
        Account ac=TestUtils.testDEConsumerAccount();
        ac.personBirthDate=Date.today()-500;
        insert ac;
        Lead testLead = new Lead();
        testLead.Phone = '123456789';
        testLead.City = 'Test City';
        testLead.Email = 'Test@gmail.com';
        testLead.Street = 'Test Street';
        testLead.PostalCode = '12345';
        testLead.Country = 'Germany';        
        
        testLead.Sample_Program_Lead_Type__c='DTC';
        testLead.LeadSource='$Hello Dexcom';       
        
        testLead.firstname='Testfirstname';
        testLead.lastname='Testlastname';
        testLead.Email='Test2@gmail.com';
        testLead.Birth_Date__c=Date.today()-500;
        
        Test.startTest();
          insert testLead;
       test.stoptest();
    }

}