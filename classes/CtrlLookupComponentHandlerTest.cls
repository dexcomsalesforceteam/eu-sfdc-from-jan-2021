/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created   : 12/01/2021
@Description    :Test class for ClsLookupComponentHandler
****************************************************************************************************************/
@isTest 
public class CtrlLookupComponentHandlerTest {
	@isTest 
    static void getAllUsersCallout(){
    	Test.setMock(HttpCalloutMock.class, new ClsVeevaMockResponse());
		CtrlLookupComponentHandler.getVeevaUsers('All','User');
		CtrlLookupComponentHandler.responseTaskWrapper responseWrapper =new CtrlLookupComponentHandler.responseTaskWrapper();
    	responseWrapper.Name='Test001';
    	responseWrapper.Id='0000055555555';    
    }
    @isTest 
    static void getSearchUsersCallout() {
    	// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new ClsVeevaMockResponse());       
        CtrlLookupComponentHandler.getVeevaUsers('M','User'); 
         //To cover the Wrapper class
        CtrlLookupComponentHandler.responseTaskWrapper obj2=new CtrlLookupComponentHandler.responseTaskWrapper();
            obj2.Name='Test001';
            obj2.Id='0000055555555';
    }
}