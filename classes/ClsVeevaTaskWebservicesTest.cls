/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created  : 14/01/2021
@Description   : Test class for ClsVeevaTaskWebservices
****************************************************************************************************************/
@Istest
global class ClsVeevaTaskWebservicesTest {
     @IsTest
    static void makeVeevaCallOutTest()
    {
        Set<ID> tsk = new Set<ID>();
        Id taskRecordType = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Veeva Task').getRecordTypeId();
		Task newTask = new Task();
            newTask.Subject='Test Subject';
            newTask.status='Open';
            newTask.priority='test priority';
            newTask.Description ='test description';
            newTask.ActivityDate=system.today();
            newTask.RecordTypeId=taskRecordType;
            newTask.Veeva_User_Id__c='0022222222222';        
			insert  newTask;
			tsk.add(newtask.Id);
        
	Test.setMock(HttpCalloutMock.class, new ClsVeevaMockResponse());	
	ClsVeevaTaskWebservices.makeVeevaCallOut(tsk);
    }
    
    @isTest
	static void closeVeevaTasksTest() {
    List<Task> tasksToClose = new List<Task>();
    Test.setMock(HttpCalloutMock.class, new ClsVeevaMockResponse());
    Task newTask = new Task();
            newTask.Subject='Test Subject';
            newTask.status='Open';
            newTask.priority='test priority';
            newTask.Description ='test description';
            newTask.ActivityDate=system.today();
            insert newTask;
			tasksToClose.add(newTask);
    Test.startTest();
         ClsVeevaTaskWebservices.closeVeevaTasks(tasksToClose);
         Task closedTask = [Select id,status from Task where Id =: tasksToClose[0].Id];
         System.assertequals('Completed',closedTask.Status);
    Test.stopTest();

	}
    
}