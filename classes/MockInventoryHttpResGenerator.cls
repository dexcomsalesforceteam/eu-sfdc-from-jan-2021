@isTest
global class MockInventoryHttpResGenerator implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
		res.setStatus('OK');        
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"ItemsResult":[{"AvailableToTransact":6795,"ItemNumber":"STT-GS-003","ItemRevision":"","OrganizationCode":"HLNL","PrimaryUnitOfMeasure":"EA","QuantityOnhand":32740,"Subinventory":"FG-HLNL"}]}');
        res.setStatusCode(200);
        return res;
    }
}