public class ClsConstants {

    //Account Record Types
    public static final string ACCOUNT_DE_PRESCRIBER_RECORDTYPE_NAME = Label.Account_DE_Prescriber_RecordType_Name;
    public static final string ACCOUNT_CA_PRESCRIBER_RECORDTYPE_NAME = Label.Account_CA_Prescriber_RecordType_Name;
    public static final string ACCOUNT_DE_MEDICAL_FACILITY_RECORDTYPE_NAME = Label.Account_DE_Medical_Facility_RecordType_Name;
    public static final string ACCOUNT_CA_MEDICAL_FACILITY_RECORDTYPE_NAME = Label.Account_CA_Medical_Facility_RecordType_Name;
    public static final string ACCOUNT_DE_PRESCRIBER_RECORDTYPE_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Account_DE_Prescriber_RecordType_Name).getRecordTypeId();
    public static final string ACCOUNT_DE_MEDICAL_FACILITY_RECORDTYPE_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Account_DE_Medical_Facility_RecordType_Name).getRecordTypeId(); 
    
    //Order RecordTypes
    public static final string ORDER_DE_SALES_ORDER_RECORDTYPE_ID = Schema.SObjectType.Order.getRecordTypeInfosByName().get('DE Sales Order').getRecordTypeId();
    //Validation Request Record Types
    public static final string VALIDATION_REQUEST_ACTIVITY_RECORDTYPE_NAME = Schema.SObjectType.QIDC__Validation_Request_ims__c.getRecordTypeInfosByName().get('Activity').getRecordTypeId();
    public static final string VALIDATION_REQUEST_WORKPLACE_RECORDTYPE_NAME = Schema.SObjectType.QIDC__Validation_Request_ims__c.getRecordTypeInfosByName().get('Workplace').getRecordTypeId();  
    
    //Country codes
    public static final string GERMANY_COUNTRY_CODE = Label.Germany_Country_Code;
    public static final string CANADA_COUNTRY_CODE = Label.Canada_Country_Code;
    
    //VR Request Types
    public static final string REQUEST_TYPE_NEW = Label.New;
    public static final string REQUEST_TYPE_UPDATE = Label.Update;
    
    //Opportunity Stages
    public static final string OPPORTUNITY_STAGE_CLOSED = 'Closed - Complete';
    public static final string OPPORTUNITY_DOCUMENT_COLLECTION_STATUS = 'All Documents Received';
    
    //Order Status
    public static final string ORDER_STATUS_CLOSED = 'Closed';
    public static final string ORDER_STATUS_DRAFT = 'Draft';
    public static final string ORDER_STATUS_ACTIVATED = 'Activated';
    public static final string ORDER_STATUS_COMPLETED = 'completed';
    
    //Order Type
    public static final string ORDER_TYPE_DE_STANDARD = 'DE STANDARD';
    
    //Task Status
    public static final string TASK_STATUS_COMPLETED = 'Completed';
    
    
    
}