/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created    : 08/01/2021
@Description    :Salesforce-Veeva integartion callout to create Task at veeva and inbound from Veeva to close task
****************************************************************************************************************/
@RestResource(urlMapping='/ClsVeevaTaskWebservices/*')
global class ClsVeevaTaskWebservices {
    @HttpPost
    //inbound to close Veeva Tasks in Salesforce
    global static void closeVeevaTasks(List<task> tasksToClose) { 
    List<TASK> closeTasks = new List<TASK>();
    List<TASK> veevaTasks =[Select ID,Status FROM Task WHERE ID IN:tasksToClose]; 
    	if(!veevaTasks.isEmpty()) {
        	for(Task tsk :veevaTasks) {
            	tsk.Status=ClsConstants.TASK_STATUS_COMPLETED;          
            	closeTasks.add(tsk);
        	}  
        }
        try{
            if(!closeTasks.isEmpty()) {    
            	update closeTasks; 
            }
        }
        catch(DmlException de) {
            System.debug('Insert error' + de);
        }
    }
	//outbound call to create Tasks in Veeva when veeva task created at Salesforce.
	@future (callout=true) 
	public static void makeVeevaCallOut(Set<ID> tasks) {
     	List<TASK> newTasks = [Select ID,subject,What.Name,Priority,Description,Status,ActivityDate,Veeva_User_Id__c FROM TASK WHERE ID IN: tasks ];
     	if(!newTasks.isEmpty()) { 
        	JSONGenerator gen = JSON.createGenerator(true);
    		gen.writeStartObject();
			gen.writeFieldName('salesforceTasks');
        	gen.writeStartArray();
        	for(Task tsk : newTasks) {
        		gen.writeStartObject();
            	if(tsk.subject!=null) {
        			gen.writeStringField('subject', tsk.subject);
            	}
        		gen.writeStringField('priority', tsk.priority);
        		gen.writeStringField('status', tsk.status);
            	if(tsk.activityDate!=null) {
        			gen.writeDateField('activityDate',tsk.activityDate);
            	}
        		if(tsk.description!=null) {
        			gen.writeStringField('description',tsk.description);
            	}
        		gen.writeStringField('OwnerID', tsk.Veeva_User_Id__c);
        		if(tsk.What.Name!=null) {
        			gen.writeStringField('Account_Name__c', tsk.What.Name);
            	}
        		gen.writeStringField('Salesforce_TaskId__c', tsk.Id);
	        	gen.writeEndObject();
			}
        	gen.writeEndArray();
			gen.writeEndObject();
        	String requestBody =gen.getAsString();
    		system.debug('Request body is ' + requestBody);
        
			Http http = new Http();
        	HttpRequest request = new HttpRequest();
       	 	request.setEndpoint('callout:VeevaNamedCredentials/services/apexrest/SfdcTaskWebServices/createTask');
        	request.setMethod('POST');
        	request.setHeader('Content-Type','application/json');
       	 	request.setBody(requestBody);
        	request.setTimeout(20000); 
        	try {
         		HttpResponse response = http.send(request);
            	System.debug('======Veeva Response======='+response.getBody());
        	}catch(System.CalloutException e) {
        		System.debug('Callout error: '+ e);
        	}
        }
	}
}