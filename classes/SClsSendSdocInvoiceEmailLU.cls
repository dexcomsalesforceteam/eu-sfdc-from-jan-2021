/*****************************************************************************************
@Description    : Scheduler class for Benelux Email Invoice Automation
****************************************************************************************/
global class SClsSendSdocInvoiceEmailLU implements Schedulable {
    global void execute(SchedulableContext SC) {
        BClsSendSdocInvoiceEmailAllCountry sch = new BClsSendSdocInvoiceEmailAllCountry('LU');
        Database.executebatch(sch, 1);
    }
}