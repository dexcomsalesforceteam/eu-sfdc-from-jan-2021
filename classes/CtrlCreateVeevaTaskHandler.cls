/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created  : 11/01/2021
@Description   : Handler class to create task from lightning component
****************************************************************************************************************/
public class CtrlCreateVeevaTaskHandler {
    //create a Veeva Task from component
    @Auraenabled
    public static void createTask(Task task){
    	Id recordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Veeva Task').getRecordTypeId();
      	task.RecordTypeId = recordTypeID;
        try{
        	insert task;   
        }
        catch(dmlexception de){
          System.debug('Task Insert error: '+ de);  
        }
    }
}