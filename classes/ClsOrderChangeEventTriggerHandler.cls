/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created    : 18/12/2020
@Description    :Order change event trigger Handler for Order change events
****************************************************************************************************************/
public class ClsOrderChangeEventTriggerHandler {
    //Method to close opportunity when all associated Orders are closed.
    public static void closeOportunityOnOrderClosure(set<STRING> orderIdsForOpportunityUpdate){
    List<ORDER> closedOrders  = [Select ID,Type,Opportunity.ID FROM ORDER WHERE ID IN: orderIdsForOpportunityUpdate AND Type LIKE '%DE%' ];  
    Set<ID> oppIds = new Set<ID>();
    List<Opportunity> oppsToClose = new List<Opportunity>();
    	for(Order ord: closedOrders){
            oppIds.add(ord.OpportunityId);
        }
    	if(!oppIds.isEmpty()){ 
        	List<Opportunity> oppList = [Select ID,StageName,(Select ID,Status FROM Orders WHERE Status ='Draft' OR 
                                                              Status ='Shipping Hold') FROM Opportunity WHERE ID IN:oppIds];
            if(!oppList.isEmpty()){
        		for(Opportunity opp:oppList){
                	system.debug('Order size' + opp.Orders.size());
                	if(opp.Orders.size()==0){
                        opp.Doc_Collection_Status__c = clsConstants.OPPORTUNITY_DOCUMENT_COLLECTION_STATUS;
            	   		opp.StageName = clsConstants.OPPORTUNITY_STAGE_CLOSED;
            	   		oppsToClose.add(opp);
                	}      
         		}
        	}
        }       
        try{
        	if(!oppsToClose.isEmpty()){
            	update oppsToClose;
            }
        }catch(system.DmlException de){
        	System.debug('The following exception has occurred: ' + de.getMessage());
        }
	}
}