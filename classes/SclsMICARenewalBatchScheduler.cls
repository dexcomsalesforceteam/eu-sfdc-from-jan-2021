global class SclsMICARenewalBatchScheduler implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new BclsMarketingInteractionCARenewalBatch(), 10);
    }
}