public class PEClsHandlerOCEEvent {
    public static void publishOCEAccOrderEvent(String OCEOrderRefNumber,String OrderNumber,String ordStatus,String OrdtrackingNumber,String IntegrationStatus,String IntegrationMsg){
        System.debug('**SPEOUT--OCE OutEvent Publish Method Call--');
        try{
            Sample_Order_Response_Event__e evtOut = new Sample_Order_Response_Event__e();            
            evtOut.OCEOrderReference__c = OCEOrderRefNumber;
            evtOut.OrderNumber__c = OrderNumber;
            evtOut.OrderStatus__c = ordStatus;                        
            evtOut.TrackingUrl__c = OrdtrackingNumber;
            evtOut.IntegrationErrorMessage__c = IntegrationMsg;
            evtOut.IntegrationStatus__c = IntegrationStatus;            
            Database.SaveResult svrs = EventBus.publish(evtOut);
            if(svrs.isSuccess()){
                System.debug('**SPEOUT--Event Published Successfully');
                ErrorUtility.LogError('Publish', 'PEClsHandlerOCEEvent', 'Writing Event into OCE Out Channel', 'Order', 'crm', 'Succesfully published Event', evtOut.OCEOrderReference__c, 'Informational');
            }else{
                System.debug('**SPEOUT--Event Failed to Published ');
                ErrorUtility.LogErrorWithEvent('Publish', 'PEClsHandlerOCEEvent','Writing Event into OCE Out Channel', 'Order', 'crm', 'Failed to Publish Event. Error: ' + svrs.getErrors(), evtOut.OrderNumber__c, 'Fatal Error - Bailing out', String.valueOf(evtOut));
            }
        }catch(Exception ex){
            System.debug('**SPEOUT--Error--'+ex);
            System.debug('**SPEOUT--ErrorLine--'+ex.getLineNumber());            
        }       
    }
    public class oceEvent {
        public String OCEOrderRefNumber;
        public String OrderNumber;
        public String ordStatus;
        public String OrdtrackingNumber;
        public String IntegrationStatus;
        public String IntegrationMsg;
    }
    public static void publishOCEAccOrderEvent(List<oceEvent> oceEvents){
        System.debug('**SPEOUT--OCE OutEvent Publish Method Call--');
        List<Sample_Order_Response_Event__e> evtOutList = new List<Sample_Order_Response_Event__e>();
        for (oceEvent evt : oceEvents) {
            Sample_Order_Response_Event__e evtOut = new Sample_Order_Response_Event__e();            
            evtOut.OCEOrderReference__c = evt.OCEOrderRefNumber;
            evtOut.OrderNumber__c = evt.OrderNumber;
            evtOut.OrderStatus__c = evt.ordStatus;                        
            evtOut.TrackingUrl__c = evt.OrdtrackingNumber;
            evtOut.IntegrationErrorMessage__c = evt.IntegrationMsg;
            evtOut.IntegrationStatus__c = evt.IntegrationStatus; 
            evtOutList.add(evtOut);
        }
        
        try{
            Database.SaveResult[] svrsList = EventBus.publish(evtOutList);
            integer i = -1;
            for(Database.SaveResult svrs : svrsList) {
                i++;
                if(svrs.isSuccess()){
                    System.debug('**SPEOUT--Bulk-Event Published Successfully');
                    ErrorUtility.LogError('Publish', 'PEClsHandlerOCEEvent', 'Writing Event into OCE Out Channel', 'Order', 'crm', 'Succesfully published Event', evtOutList[i].OCEOrderReference__c, 'Informational');
                }else{
                    System.debug('**SPEOUT--Event Failed to Published ');
                    ErrorUtility.LogErrorWithEvent('Publish', 'PEClsHandlerOCEEvent','Writing Event into OCE Out Channel', 'Order', 'crm', 'Failed to Publish Event. Error: ' + svrs.getErrors(), evtOutList[i].OrderNumber__c, 'Fatal Error - Bailing out', String.valueOf(evtOutList[i]));
                }
            }
        }catch(Exception ex){
            System.debug('**SPEOUT--Error--'+ex);
            System.debug('**SPEOUT--ErrorLine--'+ex.getLineNumber());            
        }       
    }
}