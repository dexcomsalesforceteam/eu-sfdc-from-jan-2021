global class SClsMarketingInteractionCARenewalBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        BclsMarketingInteractionCARenewalBatch caRenewBatch = new BclsMarketingInteractionCARenewalBatch();
        Database.executebatch(caRenewBatch, 20);
    }
    /*SClsMarketingInteractionCARenewalBatch caMICARenewBatch = new SClsMarketingInteractionCARenewalBatch();
    // Schedule to run at 12PM European timezone, which is 5AM Canadian time zone
    String sch = '0 0 12  * * ? *';
    String jobID = System.schedule('CARenewBatch5AMRun', sch, caMICARenewBatch);*/
}