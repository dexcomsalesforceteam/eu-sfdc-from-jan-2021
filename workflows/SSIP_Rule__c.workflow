<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Pending_Payments_to_Zero</fullName>
        <description>Set Subscription Pending Payments to 0 if the Rule is Activated or a new Rule is Added.</description>
        <field>Subscription_Pending_Payments__c</field>
        <formula>0</formula>
        <name>Set Pending Payments to Zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SSIP_Product_Name</fullName>
        <field>Product_Name__c</field>
        <formula>Product_Name__c +  TEXT(NOW())</formula>
        <name>Update SSIP Product Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Pending Payments</fullName>
        <actions>
            <name>Set_Pending_Payments_to_Zero</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SSIP_Rule__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CA SSIP Rule</value>
        </criteriaItems>
        <criteriaItems>
            <field>SSIP_Rule__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>SSIP_Rule__c.Product_Name__c</field>
            <operation>startsWith</operation>
            <value>MT</value>
        </criteriaItems>
        <description>Update pending payments to 0 when a payment SKU is made Active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update SSIP Rule Product Name</fullName>
        <actions>
            <name>Update_SSIP_Product_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SSIP_Rule__c.Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <description>Update Product Name when the SSIP Status is changed to inactive</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>