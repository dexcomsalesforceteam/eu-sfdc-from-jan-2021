({
	showSuccess : function(component, event) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: 'New Task Created',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'        
        });
        toastEvent.fire(); 
    }
})