({
	//function to send captured form details to server
    createNewTask: function(component, event, helper) {
    var newTask = component.get("v.newTask");
    var whatId = component.get("v.recordId");
    newTask.WhatId = whatId;
        if(component.get("v.selectedLookUpRecord").Id != undefined){
           newTask.Veeva_User_Id__c  = component.get("v.selectedLookUpRecord").Id;
		}
    	if(component.get("v.selectedLookUpRecord").Name != undefined){
           newTask.Veeva_User__c = component.get("v.selectedLookUpRecord").Name;
		} 
     	var action = component.get("c.createTask");
     	action.setParams({
     		"task": newTask
		});
    	action.setCallback(this,function(response){
        var state = response.getState();
        	if(state === "SUCCESS"){
                helper.showSuccess(component, event, helper);
        	}
        	else{
        		console.log("Failed with state "+state);
        	} 
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();
    	});
    	$A.enqueueAction(action);    	
	}
})