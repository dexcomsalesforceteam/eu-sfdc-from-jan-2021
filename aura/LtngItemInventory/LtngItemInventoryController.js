({
	doInit : function(component, event, helper){ 
        component.set('v.isSuccesResult',false);
        helper.onloadPriceBookentries(component, event);
	},
    ClearValues: function(component, event, helper){ 
    	component.set('v.isSuccesResult',false);
        var ordItems = component.get('v.lstOfOrderItems');
        for(var i=0;i<ordItems.length;i++){
            if(ordItems[i].selected == true){
                ordItems[i].selected = false;
            } 
        }
    },
    getItemInventory: function(component, event, helper){ 
    	var selectedtLists = [];
        var ordItems = component.get('v.lstOfOrderItems');
        for(var i=0;i<ordItems.length;i++){
            if(ordItems[i].selected == true){
                selectedtLists.push(ordItems[i].ProductCode);
            } 
        }
        if(selectedtLists.length>0){
            var ordRecordId = component.get("v.recordId");
            var action = component.get("c.getAvailableQuantity"); 
            action.setParams({'lstOfItems' : selectedtLists,
                              'OrderId':ordRecordId});
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    //alert('State--'+response.getReturnValue());
                    component.set('v.isSuccesResult',true);
                    component.set('v.lstItemInventoryRes',response.getReturnValue());
                    
                }else if (state === "ERROR") {
                    var errors = response.getError();
                }  
            });
            $A.enqueueAction(action);
        }else{
           helper.showWarning(component,event); 
        }
        
    },    
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    }
})