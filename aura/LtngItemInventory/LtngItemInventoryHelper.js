({
	onloadPriceBookentries : function(component, event, helper) { 
        var ordRecordId = component.get("v.recordId");
        var action = component.get("c.getPriceBookEntries"); 
        action.setParams({'OrderId':ordRecordId });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if (state === "SUCCESS") {
             component.set('v.lstOfOrderItems',response.getReturnValue());                              
            }else if (state === "ERROR") {
                var errors = response.getError();
            }  
        });
        $A.enqueueAction(action);
    },
    showWarning : function(component, event, helper) { 
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Warning',
            message: 'Please select Item to get Inventory.',
            duration:' 5000',
            key: 'info_alt',
            type: 'warning',
            mode: 'sticky'
        });
        toastEvent.fire();
    }
})