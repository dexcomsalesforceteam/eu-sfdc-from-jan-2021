trigger FundTrigger on Fund__c (before insert,after update,before update) {
    
  /*  if(Trigger.isInsert && Trigger.isBefore){
        new ClsFundTriggerHandler().onBeforeInsert(Trigger.new);
        ClsFundTriggerHandler.checkDuplicateActiveFund(Trigger.new);
    }
    if(Trigger.isAfter && Trigger.isUpdate){
         new ClsFundTriggerHandler().onAfterUpdate(Trigger.new,trigger.oldMap);
    } 
    
     if(Trigger.isBefore && Trigger.isUpdate){
        ClsFundTriggerHandler.checkDuplicateActiveFund(Trigger.new);
    } */
    
    if(Trigger.isInsert){
        if(Trigger.isBefore){            
            new ClsFundTriggerHandler().onBeforeInsert(Trigger.new);
           // ClsFundTriggerHandler.checkDuplicateActiveFund(Trigger.new);
        }
    }
    
    if(Trigger.isUpdate){
        if(Trigger.isBefore){
            System.debug('Before Update--');
            //ClsFundTriggerHandler.checkDuplicateActiveFund(Trigger.new);
            new ClsFundTriggerHandler().onBeforeUpdate(Trigger.new,trigger.oldMap);
        } 
        if(Trigger.isAfter){
            System.debug('After Update--');
            new ClsFundTriggerHandler().onAfterUpdate(Trigger.new,trigger.oldMap);
        }
    }
}