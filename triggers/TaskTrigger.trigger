trigger TaskTrigger on Task (after insert) {
	 //Salesforce- veeva integration create Task
    Set<Id> tskIds = new Set<Id>(); 
    ID taskRecordType = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Veeva Task').getRecordTypeId();
    for(Task vTasks : Trigger.new){
        if(vTasks.recordTypeId==taskRecordType){
     		tskIds.add(vTasks.Id); 
        }
    }
    if(Trigger.isafter && Trigger.isInsert){
		ClsTaskTriggerHandler.afterinsert(trigger.new);
        if(!tskIds.isEmpty()){
			if(System.IsBatch() == false && System.isFuture() == false){ 
         		ClsVeevaTaskWebservices.makeVeevaCallOut(tskIds);//NS-1921
          	}
		}
    }
}