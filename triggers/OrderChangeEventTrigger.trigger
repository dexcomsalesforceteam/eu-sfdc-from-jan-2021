/****************************************************************************************************************
@Author        : Pradeep Sadasivan
@Date Created    : 18/12/2020
@Description    :Order change event trigger for Order change events
****************************************************************************************************************/
trigger OrderChangeEventTrigger on OrderChangeEvent (after insert) {
	List<OrderChangeEvent> orderChanges = Trigger.new;
    Set<String> orderIdsForOpportunityUpdate = new Set<String>();
    if(!OrderChanges.isEmpty()){
        //Check if Status is Closed- NS-1788
    	for (OrderChangeEvent event : orderChanges) {
        	if(event.Status== clsConstants.ORDER_STATUS_CLOSED){
            	List<String> recordIds = event.ChangeEventHeader.getRecordIds();
                orderIdsForOpportunityUpdate.addAll(recordIds);
            }
     	}
    }
    if(!orderIdsForOpportunityUpdate.isEmpty()){ 
    	ClsOrderChangeEventTriggerHandler.closeOportunityOnOrderClosure(orderIdsForOpportunityUpdate);
    }
}